<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\QuoteItemsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\QuoteItemsTable Test Case
 */
class QuoteItemsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\QuoteItemsTable
     */
    public $QuoteItems;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.quote_items',
        'app.quotes',
        'app.clients',
        'app.schedules',
        'app.users',
        'app.user_roles',
        'app.departments',
        'app.employees',
        'app.items'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('QuoteItems') ? [] : ['className' => 'App\Model\Table\QuoteItemsTable'];
        $this->QuoteItems = TableRegistry::get('QuoteItems', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->QuoteItems);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
