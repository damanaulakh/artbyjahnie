<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\OrderLogsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\OrderLogsTable Test Case
 */
class OrderLogsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\OrderLogsTable
     */
    public $OrderLogs;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.order_logs',
        'app.orders',
        'app.quotes',
        'app.clients',
        'app.schedules',
        'app.users',
        'app.user_roles',
        'app.departments',
        'app.employees',
        'app.quote_items',
        'app.items',
        'app.statuses',
        'app.order_items'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('OrderLogs') ? [] : ['className' => 'App\Model\Table\OrderLogsTable'];
        $this->OrderLogs = TableRegistry::get('OrderLogs', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->OrderLogs);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
