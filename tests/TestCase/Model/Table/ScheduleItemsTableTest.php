<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ScheduleItemsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ScheduleItemsTable Test Case
 */
class ScheduleItemsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ScheduleItemsTable
     */
    public $ScheduleItems;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.schedule_items',
        'app.schedules',
        'app.clients',
        'app.users',
        'app.user_roles',
        'app.departments',
        'app.quotes',
        'app.employees',
        'app.quote_items',
        'app.items'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ScheduleItems') ? [] : ['className' => 'App\Model\Table\ScheduleItemsTable'];
        $this->ScheduleItems = TableRegistry::get('ScheduleItems', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ScheduleItems);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
