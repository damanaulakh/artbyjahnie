<?php
namespace App\Test\TestCase\Controller;

use App\Controller\OrderLogsController;
use Cake\TestSuite\IntegrationTestCase;

/**
 * App\Controller\OrderLogsController Test Case
 */
class OrderLogsControllerTest extends IntegrationTestCase
{

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.order_logs',
        'app.orders',
        'app.quotes',
        'app.clients',
        'app.schedules',
        'app.users',
        'app.user_roles',
        'app.departments',
        'app.employees',
        'app.quote_items',
        'app.items',
        'app.statuses',
        'app.order_items'
    ];

    /**
     * Test index method
     *
     * @return void
     */
    public function testIndex()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test view method
     *
     * @return void
     */
    public function testView()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test add method
     *
     * @return void
     */
    public function testAdd()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test edit method
     *
     * @return void
     */
    public function testEdit()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test delete method
     *
     * @return void
     */
    public function testDelete()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
