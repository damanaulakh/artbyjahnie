<?php
return [
        'number' => '<a href="{{url}}" class="btn btn-default btn-next-active">{{text}}</a>',
        'ellipsis'=>'<span class="ellipsis btn btn-success ">...</span>',
        'nextActive'=>'<a href="{{url}}" class="btn btn-default next-active">{{text}}</a>',
        'nextDisabled'=> '<a href="{{url}}" class="btn btn-info  disabled">{{text}}</a>',
        'current'=>'<a href="{{url}}" class="btn btn-default active">{{text}}</a>',
        'prevActive'=>'<a href="{{url}}" class="btn btn-default pre-next ">{{text}}</a>',
        'prevDisabled'=>'<a href="{{url}}" class="btn btn-info   disabled">{{text}}</a>'        
    ];
?>