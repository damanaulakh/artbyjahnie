<div class="wrapper">
<!-- Content Wrapper. Contains page content -->

    <div class="content-wrapper">
    <?php echo $this->Flash->render('positive'); ?>
        <section class="content-header clearfix">
            <div class="col-lg-12 heading-top">
                <h1 class="heading-text-color pull-left">Manage SubAdmin</h1>
                <button href="#" data-toggle="modal" data-backdrop="static" data-target="#Educational-info" class="btn btn-default add_button pull-right" type="button">Add Subadmin</button>
                <?php
                    echo $this->Form->Create(null, array(
                            'url' => ['action' => 'employees'],
                            'type' => 'get',
                        )
                    );
                ?>
                <div class="input-group my-search col-sm-3 pull-right">
                    <?php
                            echo $this->Form->input('search', array(
                                    'class' => 'form-control',
                                    'placeholder' =>"Search for..." ,
                                    'label'=>FALSE,
                                    'templates' => [
                                       'inputContainer' => '{{content}}',
                                       'inputContainerError' => '{{content}}{{error}}'
                                   ],
                                    'value' => (isset($_GET['search']) && !empty($_GET['search'])) ? $_GET['search'] : ''
                                )
                            );
                    ?>
                    <span class="input-group-btn"> 
                    <?= $this->Form->button(
                            __('Go!'),
                                [
                                    'type' => 'submit',
                                    'class' => 'btn btn-default'
                                ]
                            );
                    ?>
                     </span>
                </div>
                </form>
            </div>
        </section>
        <section class="content shoping-cart clearfix">
            <div class="col-sm-12">
                <div class="note-listing">
                    <div class="table-responsive">
                        <table class="table shoping-cart-table">
                            <thead>
                                <tr>
                                    <th><?php echo __($this->Paginator->sort('first_name','Subadmin Name'))?></th>
                                   
                                    <th><?php echo __($this->Paginator->sort('email','Email'))?></th>
                                    <th><?php echo __($this->Paginator->sort('phone','Phone No'))?></th>
                                    <th>Status</th>
                                    <th>&nbsp</th>
                                    <th>&nbsp</th>
                                    <th>&nbsp</th>
                                    <th>&nbsp</th>
                                    <th>&nbsp</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if(!empty($user->toArray())){
                                foreach ($user as $users) {
                            ?>
                                <tr>
                                    <td><?php echo $users->first_name.' '.$users->last_name;?></td>
                                   
                                    <td><?php echo $users->email ;?></td>
                                    <td><?php echo $users->phone ;?></td>
                                    <td><?php echo (($users->is_active) == 0) ? "Deactive" : "Active";?></td>
                                    <td>&nbsp</td>
                                    <td>&nbsp</td>
                                    <td>&nbsp</td>
                                    <td>&nbsp</td>
                                    <td>&nbsp</td>
                                    <td>
                                        
                                        <span class="switch-toggle custom-switch">
                                            <input type="hidden" value="0" id="<?php echo "test".$users->id.'_' ?>" name="data[]" checked>
                                            <input type="checkbox" hidden="hidden"  value="1" class="switch_custom" 
                                                id="<?php echo "test".$users->id ?>" 
                                                data-url="<?php echo $this->Url->build(
                                                             [      
                                                                 'controller' => 'Users',                                                
                                                                 "action" => "deactivateEmployee",
                                                                 base64_encode($users->id)
                                                             ],true) ?>"  
                                                name="data[]"
                                                <?php echo ($users->is_active) ? "checked" : ""?>>
                                            <label class="switch" for="<?php echo "test".$users->id ?>"></label>
                                        </span>
                                        
                            
                                        <?php echo $this->Html->link(
                                                    '<i class="fa fa-edit" aria-hidden="true"></i>',
                                                    'javascript:void(0)',
                                                    array(                                           
                                                            'class' => 'editEmployee',
                                                            'data-url' => $this->Url->build(
                                                             [      
                                                                 'controller' => 'Users',                                                
                                                                 "action" => "editEmployee",
                                                                 base64_encode($users->id)
                                                             ],true
                                                         ),
                                                            'escape' => false,
                                                        )
                                        );?>
                                    </td>
                                </tr>
                            <?php } } else {?>
                                <tr>
                                    <td colspan="5">
                                        No Record Found
                                    </td>
                                </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                        <?php echo $this->element('pagination'); ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<div class="modal Educational-info fade" id="edit-employee" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <a href="#" class="border_radius" data-dismiss="modal">X</a>
            <div class="modal-header">
                <h4 class="modal-title text-center">Edit Subadmin</h4>
            </div>
            <div class="modal-body">
            </div>
        </div>
    </div>
</div>
<?php echo $this->Html->script('users/editemployee',array('block' => 'scriptBottom'));?>