<div class="site-wrapper">
    <div class="site-wrapper-inner">
    <!-- Fixed navbar -->
        <div class="container">
            <div class="col-md-8 col-lg-offset-2 col-md-offset-1 login-inner-wraper padding-zero">
                <div class="background-strip-top"></div>
                <div class="col-md-5 login-left-bg  text-center">
                    <?php
                        echo $this->Html->link(
                                $this->Html->image('artbyjahnnie.png', ['alt' => 'logo']),
                                '#',
                                [
                                    'escape' => false
                                ]
                            );
                    ?>
                    <div class="call-us">
                        <div class="call-us-heading">Call us</div>
                        <div class="call-phone"><b>973-361-3900 908-368-8660</b></div>
                        <div class="address">Thor Xpress Transport, LLC 750 Walnut Ave
                        Cranford,<b>NJ 07016</b>.</div>
                    </div>
                    
                </div>
                <div class="col-md-7 login-right-bg ">
                    <div class="text-center top-right">
                        <h3><?php echo __('Welcome to Thor'); ?></h3>
                        <div class="user-login"><?php echo __('Admin Login'); ?></div>
                    </div>
                    <?php
                        echo $this->Flash->render();
                        echo $this->Form->create($user, [
                                'novalidate' => true,
                                'class' => 'login-section'
                            ]);
                    ?>
                        <div class="form-group">
                            <label><?php echo __('Email Address'); ?></label>
                            <?php
                                echo $this->Form->email('email',
                                        [
                                            'class' => 'form-control',
                                            'placeholder' => __('Email Address'),
                                            'label' => false
                                        ]
                                    );
                            ?>
                        </div>
                        <div class="form-group">
                            <label><?php echo __('Password'); ?></label>
                            <?php
                                echo $this->Form->password('password',
                                        [
                                            'class' => 'form-control',
                                            'placeholder' => __('Password'),
                                            'label' => false
                                        ]
                                    );
                            ?>
                        </div>
                        <div class="form-group">
                            <?php
                                echo $this->Html->link('Forget Password',
                                            '#',
                                            [
                                                'class' => 'forget-text'
                                            ]
                                        );
                            ?>
                        </div>
                    <?php
                        echo $this->Form->button(__('Login'), [
                                'type' => 'submit',
                                'class' => 'btn btn-black btn-block'
                            ]);
                        echo $this->Form->end();
                    ?>
                   
                </div>
            </div>
        </div>
    </div>
</div>