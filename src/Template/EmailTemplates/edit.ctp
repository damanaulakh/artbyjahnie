<!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <section class="content-header clearfix">
       <div class="col-lg-12 heading-top">
          <h1 class="heading-text-color pull-left">Email Templates</h1>
       </div>
    </section>
    <!-- Main content -->
    <section class="content">
       <div class="box">         
          <div class="col-lg-12">                     
            <div id="emails">
                <?= $this->Form->create($emailTemplate,array('class' => 'form-horizontal')) ?>
                    <div class="form-group">
                       <label class="control-label" for="exampleInputEmail1">Template Used For</label>
                       <?= $this->Form->input('template_used_for', ['class'=>'form-control', 'label' => false]) ?>
                    </div>
                    <div class="form-group">
                       <label class="control-label" for="exampleInputEmail1">Subject</label>
                       <?= $this->Form->input('subject', ['class'=>'form-control', 'label' => false]) ?>
                    </div>
                    <div class="form-group">
                        <?php echo $this->CKEditor->loadJs(); ?>
                        <label class="control-label" for="exampleInputEmail1">Body</label>
                        <?= $this->Form->input('mail_body', ['class'=>'form-control', 'label' => false]) ?>
                        <?= $this->CKEditor->replace('mail_body'); ?>
                    </div>
                    <div class="col-lg-8">
                        <?= $this->Form->button(__('Save'), ['class' => "sbtn submit-info submit_black", 'type' => 'submit']) ?>
                        <button class="btn submit-info submit_black">Cancel</button>
                    </div>
                <?= $this->Form->end() ?>
               
             </div>
          </div>
       </div>
    </section>
    <!-- /.content -->
</div>
