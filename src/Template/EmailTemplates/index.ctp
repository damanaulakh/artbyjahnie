<!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
 <?php echo $this->Flash->render('positive');?>
    <section class="content-header clearfix">
       <div class="col-lg-12 heading-top">
          <h1 class="heading-text-color pull-left">Email Templates</h1>
       </div>
    </section>
    <!-- Main content -->
    <section class="content">
       <div class="box">

            <div class="table-responsive">
            <table class="table shoping-cart-table">
                <thead>
                    <tr>
                       
                        <th><?= $this->Paginator->sort(
                                'template_used_for',
                                'Template Used For',
                                ['escape' => false]
                                )
                            ?>
                             
                        </th>
                        <th><?= $this->Paginator->sort(
                                'subject',
                                'Subject',
                                ['escape' => false]
                                )
                            ?>
                             
                        </th>                           
                      
                        <th><?= $this->Paginator->sort(
                                'modified',
                                'Modified',
                                ['escape' => false]
                                )
                            ?>
                            
                        </th>
                        <th class="actions"><?= __('Actions') ?>
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php 
                       if (!empty($emailTemplates)) {
                          foreach ($emailTemplates as $key => $emailTemplate) {
                    ?>
                          <tr>
                             <td><?= __($emailTemplate->template_used_for) ?></td>
                             <td><?= __($emailTemplate->subject) ?></td>
                             <td><?= $emailTemplate->modified ?></td>
                             <td>
                                <?= $this->Html->link(
                                        __('<span class="fa fa-edit"></span>'), 
                                        ['action' => 'edit', $emailTemplate->id], 
                                        ['escape' => false]) 
                                ?>
                            </td>
                          </tr>
                        <?php   
                                }//end of foreach loop
                            } else{
                              echo "<tr><td colspan=6>No Record found</td></tr>";
                            }
                        ?>
                    </tbody>
                </table>
                <?= $this->element('pagination'); ?>
            </div>
        </div>
    </section>
    <!-- /.content -->
 </div>
