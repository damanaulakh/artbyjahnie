<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Admin Manager Panel</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="apple-touch-icon" sizes="57x57" href="../img/small/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="../img/small/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="../img/small/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="../img/small/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="../img/small/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="../img/small/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="../img/small/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="../img/small/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="../img/small/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="../img/small/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="../img/small/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="../img/small/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="../img/small/favicon-16x16.png">
        <link rel="manifest" href="../img/small/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="../img/small/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        
        <!-- Bootstrap 3.3.5 -->
        <?php
            echo $this->Html->css([
                    'bootstrap.min',
                    'style',
                    'AdminLTE',
                    'custom-switch',
                    'bootstrap-datetimepicker.min',
                    'intlTelInput',
                    'pnotify/pnotify.custom.min',
                    'loader'
                ]);
            echo $this->fetch('meta');
            echo $this->fetch('css');
        ?>
    </head>
    <body class="hold-transition skin-blue sidebar-mini ">
        <div class="wrapper">
            <?php
                echo $this->element('frontend/header');
                echo $this->element('frontend/left_navigation');
                echo $this->fetch('content');
                echo $this->element('frontend/add_employee');

            ?>
        </div>
        <?php echo $this->element('frontend/change_password'); ?>
        <?php //echo $this->element('frontend/profile'); ?>
        <?php
            echo $this->Html->script([
                    'jQuery-2.1.4.min',
                    'bootstrap.min',
                    'jquery.validate',
                    'additional-methods',
                    'app',
                    'dropzone',
                    'moment.min',
                    'bootstrap-datetimepicker',
                    'underscore-min',
                    'intlTelInput.min',
                    'pnotify/jquery.pnotify',
                    'pnotify/index'
                ]
            );
            echo $this->fetch('scriptBottom');
        ?>
        <script>
            $(function() {
                if ($(window).width() < 1024) {
                    $("body").addClass('sidebar-collapse');
                }
                var equalto =$("body").innerHeight();
                $(".main-sidebar").innerHeight(equalto);
            });
        </script> 
   </body>
</html>