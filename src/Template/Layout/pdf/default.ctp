<!DOCTYPE html>
<html>
    <?php
        use Cake\Core\Configure;
    ?>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Admin Manager Panel</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="<?php echo Configure::read('CakePdfHostName') . 'css/bootstrap.min.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo Configure::read('CakePdfHostName') . 'css/style.css'; ?>"/>
        <link rel="stylesheet" href="<?php echo Configure::read('CakePdfHostName') . 'css/AdminLTE.css'; ?>"/>
    </head>
    <body class="hold-transition skin-blue sidebar-mini ">
        <div class="wrapper">
            <?php echo $this->fetch('content'); ?>
        </div>
   </body>
</html>