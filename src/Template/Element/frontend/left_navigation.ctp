<!-- Sidebar Left-->
<aside class="main-sidebar">
    <section class="sidebar">
        <ul class="sidebar-menu">
            <li class="treeview <?php echo (($this->request->params['controller'] == 'Users') && ($this->request->params['action'] == 'employees')) ? 'active' : '' ?>">
                <?php
                    echo $this->Html->link(
                            '<i class="fa fa-group" aria-hidden="true"></i> <span>' . __('Manage SubAdmin') . '</span>',
                            '/Users/employees',
                            [
                                'escape' => false
                            ]
                        );
                ?>
            </li>
            <li class="treeview <?php echo (($this->request->params['controller'] == 'Users') && ($this->request->params['action'] == 'employees')) ? 'active' : '' ?>">
                <?php
                    echo $this->Html->link(
                            '<i class="fa fa-group" aria-hidden="true"></i> <span>' . __('Manage Categories') . '</span>',
                            '/Users/employees',
                            [
                                'escape' => false
                            ]
                        );
                ?>
            </li>
            <li class="treeview <?php echo (($this->request->params['controller'] == 'Users') && ($this->request->params['action'] == 'employees')) ? 'active' : '' ?>">
                <?php
                    echo $this->Html->link(
                            '<i class="fa fa-group" aria-hidden="true"></i> <span>' . __('Manage Products') . '</span>',
                            '/Users/employees',
                            [
                                'escape' => false
                            ]
                        );
                ?>
            </li>
            <li class="<?php echo (($this->request->params['controller'] == 'EmailTemplates') && ($this->request->params['action'] == 'index')) ? 'active' : '' ?>">
                        <?php
                            echo $this->Html->link(
                                    '<i class="fa fa-envelope"></i> <span>' . __('Manage Email Template') . '</span>',
                                    '/EmailTemplates/index',
                                    [
                                        'escape' => false
                                    ]
                                );
                        ?>
            </li>
            <li class="<?php echo (($this->request->params['controller'] == 'CmsPages') && ($this->request->params['action'] == 'index')) ? 'active' : '' ?>">
                        <?php
                            echo $this->Html->link(
                                    '<i class="fa fa-file"></i> <span>' . __('Manage Cms Pages') . '</span>',
                                    '/CmsPages/index',
                                    [
                                        'escape' => false
                                    ]
                                );
                        ?>
            </li>
        </ul>
    </section>
</aside>