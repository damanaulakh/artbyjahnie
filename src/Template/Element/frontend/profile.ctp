<?php
                    echo $this->Form->create($user,
                            array(
                                    'url' => array(
                                            'controller' => 'Users',
                                            'action' => 'Profile'
                                        ),
                                    'class' => 'form-horizontal',
                                    'id' => 'add-Profile',
                                    'type' => 'file'
                                )
                        );
                ?>            
                    <div class="form-group">
                        <label  class="col-sm-4 control-label">Full Name</label>
                        <div class="col-sm-4">
                            <?php //echo $this->Form->hidden('id',array('value' => $user->id));?>
                            <?php
                                echo $this->Form->input('first_name',
                                        array(
                                                'class' => 'form-control',
                                                'placeholder' => __('First Name'),
                                                'div' => false,
                                                'label' => false
                                            )
                                    );
                            ?>
                        </div>
                        <div class="col-sm-4">
                            <?php
                                echo $this->Form->input('last_name',
                                        array(
                                                'class' => 'form-control',
                                                'placeholder' => __('Last Name'),
                                                'div' => false,
                                                'label' => false
                                            )
                                    );
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-4 control-label">Phone Number</label>
                        <div class="col-sm-8">
                            <?php
                                echo $this->Form->input('phone',
                                        array(
                                                'class' => 'form-control',
                                                'placeholder' => __('Type your Phone Number'),
                                                'div' => false,
                                                'label' => false
                                            )
                                    );
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-4 control-label">Email Address</label>
                        <div class="col-sm-8">
                            <?php
                                echo $this->Form->input('email',
                                        array(
                                                'class' => 'form-control',
                                                'placeholder' => __('Type your Email Address'),
                                                'div' => false,
                                                'label' => false
                                            )
                                    );
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-4 control-label">Profile Pic</label>
                        <div class="col-sm-8">
                         <input type="file" name="profile_pic" id="file" />
                        </div>
                    </div>
                    <div class="form-group bordered_group"> <label  class="col-sm-4 control-label"></label>
                        <div class="col-lg-8">
                            <button type="submit" class="btn submit-info submit_black">Submit</button>
                            <button type="button" data-dismiss="modal" class="btn submit-info submit_black">Cancel</button>
                        </div>
                    </div>
                </form>
<?php echo $this->Html->script('users/profile',array('block' => 'scriptBottom'));?>
