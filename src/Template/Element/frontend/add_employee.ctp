<div class="modal Educational-info fade" id="Educational-info" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <a href="#" class="border_radius" data-toggle="modal" data-backdrop="static" data-target="#Educational-info">X</a>
            <div class="modal-header">
                <h4 class="modal-title text-center">Add Subadmin</h4>
            </div>
            <div class="modal-body">
                           <?php
                    echo $this->Form->create('AddEmployee',
                            array(
                                    'url' => array(
                                            'controller' => 'Users',
                                            'action' => 'addEmployee'
                                        ),
                                    'class' => 'form-horizontal',
                                    'id' => 'add-employee'
                                )
                        );
                ?>            
                    <div class="form-group">
                        <label  class="col-sm-4 control-label">Full Name</label>
                        <div class="col-sm-4">
                            <?php
                                echo $this->Form->input('first_name',
                                        array(
                                                'class' => 'form-control',
                                                'placeholder' => __('First Name'),
                                                'div' => false,
                                                'label' => false
                                            )
                                    );
                            ?>
                        </div>
                        <div class="col-sm-4">
                            <?php
                                echo $this->Form->input('last_name',
                                        array(
                                                'class' => 'form-control',
                                                'placeholder' => __('Last Name'),
                                                'div' => false,
                                                'label' => false
                                            )
                                    );
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-4 control-label">Phone Number</label>
                        <div class="col-sm-8">
                            <?php
                                echo $this->Form->input('phone',
                                        array(
                                                'class' => 'form-control',
                                                'placeholder' => __('Type your Phone Number'),
                                                'div' => false,
                                                'label' => false
                                            )
                                    );
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-4 control-label">Email Address</label>
                        <div class="col-sm-8">
                            <?php
                                echo $this->Form->input('email',
                                        array(
                                                'class' => 'form-control',
                                                'placeholder' => __('Type your Email Address'),
                                                'div' => false,
                                                'label' => false
                                            )
                                    );
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-4 control-label">Create A Password</label>
                        <div class="col-sm-8">
                            <?php
                                echo $this->Form->input('password',
                                        array(
                                                'class' => 'form-control',
                                                'placeholder' => __('• • • • • •'),
                                                'div' => false,
                                                'label' => false
                                            )
                                    );
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label  class="col-sm-4 control-label">Confirm Password</label>
                        <div class="col-sm-8">
                            <?php
                                echo $this->Form->input('confirm_password',
                                        array(
                                                'class' => 'form-control',
                                                'placeholder' => __('• • • • • •'),
                                                'div' => false,
                                                'label' => false,
                                                'type' => 'password'
                                            )
                                    );
                            ?>
                        </div>
                    </div>
                    <div class="form-group bordered_group"> <label  class="col-sm-4 control-label"></label>
                        <div class="col-lg-8">
                            <button type="submit" class="btn submit-info submit_black">Submit</button>
                            <button type="button" data-dismiss="modal" class="btn submit-info submit_black">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
