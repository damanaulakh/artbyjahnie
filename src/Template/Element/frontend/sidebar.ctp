<aside class="main-sidebar">
   <section class="sidebar">
      <ul class="sidebar-menu">
         <li class="treeview <?php echo (($this->request->params['controller'] == 'Clients') && ($this->request->params['action'] == 'scheduleListing')) ? 'active' : '' ?>">
             <?php
                    echo $this->Html->link(
                            '<i class="fa fa-group" aria-hidden="true"></i> <span>' . __('My Schedule') . '</span>',
                            '/Clients/scheduleListing',
                            [
                                'escape' => false
                            ]
                        );
            ?>
         </li>
         <li class="treeview <?php echo (($this->request->params['controller'] == 'Quotes') && ($this->request->params['action'] == 'orderIndex')) ? 'active' : '' ?>">
            <?php
                    echo $this->Html->link(
                            '<i class="fa fa-group" aria-hidden="true"></i> <span>' . __('Order List') . '</span>',
                            '/Quotes/orderEmployee',
                            [
                                'escape' => false
                            ]
                        );
            ?>
         </li>
      </ul>
   </section>
</aside>