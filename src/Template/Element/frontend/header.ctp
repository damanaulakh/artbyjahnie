<?php
    use Cake\Core\Configure;
    use Cake\I18n\Time;
?>
<header class="main-header">
    <!-- Logo -->
    <?php
        echo $this->Html->link(
                '<span class="logo-mini">' . $this->Html->image('artbyjahnnie.png', ['alt' => 'logo']) . '</span>
                <span class="logo-lg">' . $this->Html->image('artbyjahnnie.png', ['alt' => 'logo']) . '</span>',
                '/Users/employees',
                [
                    'class' => 'logo',
                    'escape' => false
                ]
            );
    ?>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top nav-custom">
        <!-- Sidebar toggle button-->
        <?php echo $this->Html->link(
                    '<span class="glyphicon glyphicon-menu-hamburger"></span>',
                    '#',
                    [
                        'class' => 'sidebar-toggle',
                        'data-toggle' => 'offcanvas',
                        'role' => 'button',
                        'escape' => false
                    ]
                );
        ?>
        <div class="col-sm-6  col-md-5 col-lg-4 col-lg-offset-3 search-top">
            <div class="right-inner-addon">
              
                    <h2><?php echo __('Admin Panel'); ?></h2>
                  
            </div>
        </div>
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
               
                <li class="dropdown user user-menu">
                <?php //pr($this->request->session()->read('Auth.User'));die; ?>
                    <?php echo $this->Html->link(
                            (($this->request->session()->read('Auth.User.profile_pic')) ? $this->Html->image('profile/'.$this->request->session()->read('Auth.User.profile_pic'), 
                                ['class' => 'user-image', 'alt' => 'User Image']
                                ) : $this->Html->image('images.jpeg', 
                                ['class' => 'user-image', 'alt' => 'User Image']
                                ))  . 
                            '<span class="hidden-xs hidden-sm">'.$this->request->session()->read('Auth.User.first_name').' '.$this->request->session()->read('Auth.User.last_name').'</span> <i class="fa fa-angle-down" aria-hidden="true"></i>',
                            '#',
                            [
                                'escape' => false,
                                'class' => 'dropdown-toggle',
                                'data-toggle' => 'dropdown'
                            ]
                        );
                    ?>
                    <ul aria-labelledby="drop3" class="dropdown-menu">
                        <li>
                            <?php echo $this->Html->link(__('Profile'),
                                                    'javascript:void(0)',
                                                    array(                                           
                                                            'class' => 'editprofile',
                                                            'data-url' => $this->Url->build(
                                                             [      
                                                                 'controller' => 'Users',                                                
                                                                 "action" => "profile"
                                                               
                                                             ],true
                                                         ),
                                                            'escape' => false,
                                                        )
                                        );?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(__('Change Password'),
                                        '#',
                                        array(
                                                    'data-toggle' => 'modal',
                                                    'data-target' => '#changePasswordModal',
                                                    'escape' => false
                                            )
                                    );
                            ?>
                        </li>
                        <li>
                            <?php echo $this->Html->link(__('Logout'),
                                        '/Users/logout'
                                    );
                            ?>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<div class="modal Educational-info fade" id="changeProfile" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <a href="#" class="border_radius" data-dismiss="modal">X</a>
            <div class="modal-header">
                <h4 class="modal-title text-center">Update Profile</h4>
            </div>
            <div class="modal-body">
             </div>
        </div>
    </div>
</div>
<?php echo $this->Html->script('users/profile',array('block' => 'scriptBottom'));?>
