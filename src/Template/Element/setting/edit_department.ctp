 <?php echo $this->Form->create($department,array('url'=>array('action' => 'editDepartment'),'type' => 'post','id' => 'educationinfo','class'=>'form-horizontal'))?>
        <div class="form-group">
            <label  class="col-sm-4 control-label">Department Name</label>
            <div class="col-sm-8">
                 <?php
                            echo $this->Form->hidden('id');
                 ?>
                <?php echo $this->Form->input('name',array('class' => 'form-control','label' => false));?>
            </div>
        </div>
        <div class="form-group bordered_group"> <label  class="col-sm-4 control-label"></label>
            <div class="col-lg-8">
                <button type="submit" class="btn submit-info submit_black">update</button>
                <button type="button" data-dismiss="modal" class="btn submit-info submit_black">Cancel</button>
            </div>
        </div>
</form>
            