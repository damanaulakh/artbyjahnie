 <div class="modal Educational-info schedule fade" id="add-status" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <a href="#" class="border_radius" data-toggle="modal" data-dismiss="modal">X</a>
            <div class="modal-header">
                <h4 class="modal-title text-center"><?php echo __('Add Status'); ?></h4>
            </div>
            <div id="myTabContent" class="tab-content">
                <div class="tab-pane fade active in" id="home">
                    <div class="modal-body">
                        <?php
                            echo $this->Form->create('status',
                                    [
                                        'id' => 'add-status',
                                        'class' => 'form-horizontal',
                                        'url' => [
                                            'controller' => 'Settings',
                                            'action' => 'addStatus'
                                        ]
                                    ]
                                );
                        ?>
                            <div class="form-group">
                                <label  class="col-sm-4 control-label"><?php echo __('Status'); ?></label>
                                <div class="col-sm-8">
                                   <?php
                                        echo $this->Form->input('name',
                                                array(
                                                        'class' => 'form-control',
                                                        'placeholder' => __('Status'),
                                                        'div' => false,
                                                        'label' => false
                                                    )
                                            );
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-4 control-label"><?php echo __('Department'); ?></label>
                                <div class="col-sm-8">
                                    <?php
                                echo $this->Form->input('department_id',
                                        array(
                                                 'options' => $department,
                                                 'class' => 'form-control',
                                                 'empty' => __('Select department'),
                                                 'div' => false,
                                                 'label' => false
                                           )
                                    );
                            ?>
                                </div>
                            </div>
                            <div class="form-group bordered_group"> 
                                <label  class="col-sm-4 control-label"></label>
                                <div class="col-lg-8">  
                                    <?php
                                        echo $this->Form->button(__('Submit'), [
                                                'type' => 'submit',
                                                'class' => 'btn submit-info submit_black'
                                            ]
                                        );
                                    ?>
                                    <?php
                                        echo $this->Form->button(__('Cancel'), [
                                                'type' => 'button',
                                                'class' => 'btn submit-info submit_black',
                                                'data-dismiss' => 'modal'
                                            ]
                                        );
                                    ?>
                                </div>
                            </div>
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
