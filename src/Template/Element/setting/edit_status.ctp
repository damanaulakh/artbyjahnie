<?php
                            echo $this->Form->create($status,
                                    [
                                        'id' => 'edit-status',
                                        'class' => 'form-horizontal',
                                        'url' => [
                                            'controller' => 'Settings',
                                            'action' => 'editStatus'
                                        ]
                                    ]
                                );
                        ?>
                            <div class="form-group">
                                <label  class="col-sm-4 control-label"><?php echo __('Status'); ?></label>
                                <div class="col-sm-8">
                                <?php echo $this->Form->hidden('id',array('value' => $status->id));?>
                                   <?php
                                        echo $this->Form->input('name',
                                                array(
                                                        'class' => 'form-control',
                                                        'placeholder' => __('Status'),
                                                        'div' => false,
                                                        'label' => false
                                                    )
                                            );
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-4 control-label"><?php echo __('Department'); ?></label>
                                <div class="col-sm-8">
                                    <?php
                                echo $this->Form->input('department_id',
                                        array(
                                                 'options' => $department,
                                                 'class' => 'form-control',
                                                 'empty' => __('Select department'),
                                                 'div' => false,
                                                 'label' => false
                                           )
                                    );
                            ?>
                                </div>
                            </div>
                            <div class="form-group bordered_group"> 
                                <label  class="col-sm-4 control-label"></label>
                                <div class="col-lg-8">  
                                    <?php
                                        echo $this->Form->button(__('Submit'), [
                                                'type' => 'submit',
                                                'class' => 'btn submit-info submit_black'
                                            ]
                                        );
                                    ?>
                                    <?php
                                        echo $this->Form->button(__('Cancel'), [
                                                'type' => 'button',
                                                'class' => 'btn submit-info submit_black',
                                                'data-dismiss' => 'modal'
                                            ]
                                        );
                                    ?>
                                </div>
                            </div>
                        <?php echo $this->Form->end(); ?>