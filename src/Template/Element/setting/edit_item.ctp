<?php echo $this->Form->create($item,array('url' => array('action' => 'editItem'),'type' => 'post','id' => 'educationinfo','class' => 'form-horizontal'));?>
<div class="form-group">
    <label class="col-sm-4 control-label">Item Type</label>
    <div class="col-sm-2">
           <div class="radio">
           <?php echo $this->Form->hidden('id'); ?> 
            <?php echo $this->Form->select('type', ['PIF'=> 'PIF','PS' =>'PS'],['multiple' => 'checkbox']); ?>
           </div>
    </div>
    
 </div>
     <div class="form-group">
        <label  class="col-sm-4 control-label">Name Of Item</label>
        <div class="col-sm-8">
            <?php echo $this->Form->input('name', array('class' => 'form-control','label' => false)); ?>
        </div>
     </div>
      <div class="form-group">
        <label  class="col-sm-4 control-label">Item Description</label>
        <div class="col-sm-8">
           <?php echo $this->Form->input('description', array('class' => 'form-control','label' => false,'type' => 'textarea','cols' => 6)); ?>
        </div>
     </div>
     <div class="form-group">
        <label  class="col-sm-4 control-label">Per Price</label>
        <div class="col-sm-8 padding-left-none">
        <div class="col-sm-4 posi padding-right-none">
            <?php echo $this->Form->input('per_price', array('class' => 'form-control','label' => false)); ?>
            <span>JOD</span>
        </div>
            <label  class="col-sm-4 control-label text-right">Per Meter</label>
            <div class="col-sm-4 padding-left-none padding-right-none posi ">
           <?php echo $this->Form->input('per_meter', array('class' => 'form-control','label' => false)); ?>
            <span>JOD</span>
        </div>
        </div>
       
     </div>
     <div class="form-group">
        
     </div>
     <div class="form-group">
        <label  class="col-sm-4 control-label">Per Sq. Meter+Linear Meter</label>
        <div class="col-sm-5 posi">
           <?php echo $this->Form->input('meter_linear_meter', array('class' => 'form-control','label' => false)); ?>
           <span>JOD</span>
        </div>
     </div>
     <div class="form-group">
        <label  class="col-sm-4 control-label posi">Per Linear Meter</label>
        <div class="col-sm-5 posi">
           <?php echo $this->Form->input('per_linear_meter', array('class' => 'form-control','label' => false)); ?>
           <span>JOD</span>s
        </div>
     </div>
    <div class="form-group bordered_group"> <label  class="col-sm-4 control-label"></label>
       <div class="col-lg-8">  
            <button type="submit" class="btn submit-info submit_black">
                update
            </button>
            <button type="button" data-dismiss="modal" class="btn submit-info submit_black">
                Cancel
            </button>
     </div>
    </div>
</form>
       