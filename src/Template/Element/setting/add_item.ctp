<div class="modal Educational-info fade" id="add-item" tabindex="-1" role="dialog">
 <div class="modal-dialog">
    <div class="modal-content">
     <a href="#" class="border_radius" data-toggle="modal" data-backdrop="static" data-target="#Educational-info">X</a>
       <div class="modal-header">
          <h4 class="modal-title text-center">Add New Item</h4>
       </div>
       <div class="modal-body">
          <?php echo $this->Form->create('item',array('url' => array('action' => 'addItem'),'type' => 'post','id' => 'additem','class' => 'form-horizontal'));?>
            <div class="form-group">
                <label class="col-sm-4 control-label">Item Type</label>
                <div class="col-sm-2">
                       <div class="radio">
                       <label> 
                            <input type="checkbox" name="type[]" id="optionsRadios1" value="PS" checked="" class=""> 
                            PS
                        </label> 
                       </div>
                </div>
                <div class="col-sm-2">
                    <div class="radio "> 
                            <label> 
                                <input type="checkbox" name="type[]" id="optionsRadios2" value="PIF">
                                PIF 
                            </label> 
                    </div>
                </div>
             </div>
             <div class="form-group">
                <label  class="col-sm-4 control-label">Name Of Item</label>
                <div class="col-sm-8">
                   <input type="text" name = "name" class="form-control" placeholder="Item Name ">
                </div>
             </div>
              <div class="form-group">
                <label  class="col-sm-4 control-label">Item Description</label>
                <div class="col-sm-8">
                   <textarea type="text" cols="6" name ="description" class="form-control" placeholder="Description Here"></textarea>
                </div>
             </div>
             <div class="form-group">
                <label  class="col-sm-4 control-label">Per Price</label>
                <div class="col-sm-8 padding-left-none">
                <div class="col-sm-4 posi padding-right-none">
                    <input type="text" name="per_price" class="form-control" placeholder="Price">
                    <span>JOD</span>
                </div>
                    <label  class="col-sm-4 control-label text-right">Per Meter</label>
                    <div class="col-sm-4 padding-left-none padding-right-none posi ">
                   <input type="text" name="per_meter" class="form-control" placeholder="Price">
                    <span>JOD</span>
                </div>
                </div>
               
             </div>
             <div class="form-group">
                
             </div>
             <div class="form-group">
                <label  class="col-sm-4 control-label">Per Sq. Meter+Linear Meter</label>
                <div class="col-sm-5 posi">
                   <input type="text" name="meter_linear_meter" class="form-control" placeholder=" price">
                   <span>JOD</span>
                </div>
             </div>
             <div class="form-group">
                <label  class="col-sm-4 control-label posi">Per Linear Meter</label>
                <div class="col-sm-5 posi">
                   <input type="text" name="per_linear_meter" class="form-control" placeholder="price">
                   <span>JOD</span>s
                </div>
             </div>
           <div class="form-group bordered_group"> <label  class="col-sm-4 control-label"></label>
               <div class="col-lg-8">  
                    <button type="submit" class="btn submit-info submit_black">
                        Submit
                    </button>
                    <button type="button" data-dismiss="modal" class="btn submit-info submit_black">
                        Cancel
                    </button>
             </div>
            </div>
          </form>
       </div>
    </div>
 </div>
</div>
<?php echo $this->Html->script('department/edit',array('block' => 'scriptBottom'));?>