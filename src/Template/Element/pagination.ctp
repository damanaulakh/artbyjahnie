<div>
    <div class="btn-toolbar">
        <div class="btn-group pull-right">
                <?php
                //     echo $this->Paginator->first('<');
                    if ($this->Paginator->hasPrev() ){
                        echo $this->Paginator->prev();
                    }
                    echo $this->Paginator->numbers([
                            'first' => 1,
                            'last' => 1,
                            'modulus'=>1
                        ]);    
                    if ($this->Paginator->hasNext() ){
                        echo $this->Paginator->next();
                    }
                    // echo $this->Paginator->last('>');
                ?>
        </div>
    </div>
</div>