<div style="color: #fff;">
			    <?php 
			    if(!empty($order)){
			         ?>
			        <div class="form-group clearfix">
			            <div for="address" class="col-sm-6 control-label"><?php echo __('Employee Name'); ?></div>
			            <div for="address" class="col-sm-6 control-label"><?php echo $order->employee_name ?></div>
			        </div>
			        <div class="form-group clearfix">
			            <div for="address" class="col-sm-6 control-label"><?php echo __('Client Name'); ?></div>
			            <div for="address" class="col-sm-6 control-label"><?php echo $order->client_name ?></div>
			        </div>
			        <div class="form-group clearfix">
			            <div for="address" class="col-sm-6 control-label"><?php echo __('Schedule date'); ?></div>
			            <div for="address" class="col-sm-6 control-label">$<?php echo date('d-m-Y',strtotime($order->date)); ?></div>
			        </div>
			        <div class="form-group clearfix">
			            <div for="address" class="col-sm-6 control-label"><?php echo __('Billing Address'); ?></div>
			            <div for="address" class="col-sm-6 control-label"><?php echo $order->billing_address ; ?></div>
			        </div>
			         <div class="form-group clearfix">
			            <div for="address" class="col-sm-6 control-label"><?php echo __('Shipping Address'); ?></div>
			            <div for="address" class="col-sm-6 control-label"><?php echo $order->shipping_address ; ?></div>
			        </div>
			        <div class="form-group clearfix">
			            <div for="address" class="col-sm-6 control-label"><?php echo __('Status'); ?></div>
			            <div for="address" class="col-sm-6 control-label"><?php echo $order->status->name ; ?></div>
			        </div>
			        	<?php 
				        foreach ($order->order_items as $ordersitem) { ?>
				        	<div class="form-group clearfix">
					            <div for="address" class="col-sm-6 control-label"><?php echo __('Item Name'); ?></div>
					            <div for="address" class="col-sm-6 control-label"><?php echo $ordersitem->item->name ; ?></div>
			        		</div>
			        		<div class="form-group clearfix">
					            <div for="address" class="col-sm-6 control-label"><?php echo __('quantity'); ?></div>
					            <div for="address" class="col-sm-6 control-label"><?php echo $ordersitem->quantity ; ?></div>
			        		</div>
			        		<div class="form-group clearfix">
					            <div for="address" class="col-sm-6 control-label"><?php echo __('unit'); ?></div>
					            <div for="address" class="col-sm-6 control-label"><?php echo $ordersitem->unit ; ?></div>
			        		</div>
			        		<div class="form-group clearfix">
					            <div for="address" class="col-sm-6 control-label"><?php echo __('File'); ?></div>
					            <div for="address" class="col-sm-6 control-label"><?php echo $this->Html->image($ordersitem->file_path) ; ?></div>
			        		</div>
			        		<div class="form-group clearfix">
					            <div for="address" class="col-sm-6 control-label"><?php echo __('Unit Price'); ?></div>
					            <div for="address" class="col-sm-6 control-label"><?php echo $ordersitem->unit_price ; ?></div>
			        		</div>
			        		<hr>
				       <?php }
				         ?>
			      <?php }  
			    ?>
				<div>
 			