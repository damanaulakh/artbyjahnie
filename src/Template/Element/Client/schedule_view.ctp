<div style="color: #fff;">
			    <?php 
			    if(!empty($schedule)){
			         ?>
			        <div class="form-group clearfix">
			            <div for="address" class="col-sm-6 control-label"><?php echo __('Employee Name'); ?></div>
			            <div for="address" class="col-sm-6 control-label"><?php echo $schedule->user->first_name.' '.$schedule->user->last_name; ?></div>
			        </div>
			        <div class="form-group clearfix">
			            <div for="address" class="col-sm-6 control-label"><?php echo __('Client Name'); ?></div>
			            <div for="address" class="col-sm-6 control-label"><?php echo $schedule->client->name ?></div>
			        </div>
			        <div class="form-group clearfix">
			            <div for="address" class="col-sm-6 control-label"><?php echo __('Schedule date'); ?></div>
			            <div for="address" class="col-sm-6 control-label">$<?php echo date('d-m-Y',strtotime($schedule->date)); ?></div>
			        </div>
			        <div class="form-group clearfix">
			            <div for="address" class="col-sm-6 control-label"><?php echo __('Order type'); ?></div>
			            <div for="address" class="col-sm-6 control-label"><?php echo ($schedule->order_type == 1) ? "PS" : "PIF" ; ?></div>
			        </div>
			        	<?php 
				        foreach ($schedule->schedule_items as $scheduleitem) { ?>
				        	<div class="form-group clearfix">
					            <div for="address" class="col-sm-6 control-label"><?php echo __('Item Name'); ?></div>
					            <div for="address" class="col-sm-6 control-label"><?php echo $scheduleitem->item->name ; ?></div>
			        		</div>
			        		
				       <?php }
				         ?>
			      <?php }  
			    ?>
			    <div class="form-group bordered_group"> 
                                <div class="col-lg-8 pull-right">  
                                    <?php
                                        echo $this->Form->button(__('Close'), [
                                                'type' => 'button',
                                                'class' => 'btn submit-info submit_black',
                                                'data-dismiss' => 'modal'
                                            ]
                                        );
                                    ?>
                                </div>
                  
 			