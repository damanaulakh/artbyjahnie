 <div class="modal Educational-info schedule fade" id="Educational-info" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <a href="#" class="border_radius" data-toggle="modal" data-dismiss="modal">X</a>
            <div class="modal-header">
                <h4 class="modal-title text-center"><?php echo __('Client Order'); ?></h4>
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#home" data-toggle="tab"><?php echo __('New Client'); ?></a></li>
                    <li><a href="#profile" data-toggle="tab"><?php echo __('Existing Client'); ?></a></li>
                </ul>
            </div>
            <div id="myTabContent" class="tab-content">
                <div class="tab-pane fade active in" id="home">
                    <div class="modal-body">
                       <h4 class="modal-title text-center"><?php echo __('Add New Client Order'); ?></h4>
                        <?php
                            echo $this->Form->create($clientObj,
                                    [
                                        'id' => 'add-schedule',
                                        'class' => 'form-horizontal',
                                        'url' => [
                                            'controller' => 'Clients',
                                            'action' => 'addSchedule'
                                        ]
                                    ]
                                );
                        ?>
                            <div class="form-group">
                                <label  class="col-sm-4 control-label"><?php echo __('Type Of Order'); ?></label>
                                <div class="col-sm-2">
                                    <div class="radio">
                                        <label> 
                                            <?php
                                                echo $this->Form->radio('schedules.0.order_type', [
                                                        [
                                                            'value' => 1,
                                                            'text' => __('PS'),
                                                            'checked' => true
                                                        ]
                                                    ]);
                                            ?>
                                        </label> 
                                    </div>
                                </div>
                                <div class="col-sm-2">
                                    <div class="radio "> 
                                        <label> 
                                            <?php
                                                echo $this->Form->radio('schedules.0.order_type', [
                                                        [
                                                            'value' => 2,
                                                            'text' => __('PIF')
                                                        ]
                                                    ]);
                                            ?>
                                        </label> 
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-4 control-label"><?php echo __('Client Full Name'); ?></label>
                                <div class="col-sm-8">
                                   <?php
                                        echo $this->Form->input('name',
                                                array(
                                                        'class' => 'form-control',
                                                        'placeholder' => __('Client name'),
                                                        'div' => false,
                                                        'label' => false
                                                    )
                                            );
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-4 control-label"><?php echo __('Phone Number'); ?></label>
                                <div class="col-sm-8">
                                    <?php
                                        echo $this->Form->input('phone',
                                                array(
                                                        'class' => 'form-control',
                                                        'placeholder' => __('Client Phone Number'),
                                                        'div' => false,
                                                        'label' => false
                                                    )
                                            );
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-4 control-label"><?php echo __('Facebook URL'); ?></label>
                                <div class="col-sm-8">
                                    <?php
                                        echo $this->Form->input('fb_url',
                                                array(
                                                        'class' => 'form-control',
                                                        'placeholder' => __('Facebook URL'),
                                                        'div' => false,
                                                        'label' => false
                                                    )
                                            );
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-4 control-label"><?php echo __('Address'); ?></label>
                                <div class="col-sm-8">
                                   <?php
                                        echo $this->Form->input('address',
                                                array(
                                                        'class' => 'form-control',
                                                        'placeholder' => __('Client Address'),
                                                        'div' => false,
                                                        'label' => false
                                                    )
                                            );
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-4 control-label"><?php echo __('Zip Code'); ?></label>
                                <div class="col-sm-8">
                                   <?php
                                        echo $this->Form->input('zipcode',
                                                array(
                                                        'class' => 'form-control',
                                                        'placeholder' => __('Zip Code'),
                                                        'div' => false,
                                                        'label' => false
                                                    )
                                            );
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-4 control-label"><?php echo __('Email Address'); ?></label>
                                <div class="col-sm-8">
                                   <?php
                                        echo $this->Form->input('email',
                                                array(
                                                        'class' => 'form-control',
                                                        'placeholder' => __('Client Email Address'),
                                                        'div' => false,
                                                        'label' => false
                                                    )
                                            );
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-sm-4 control-label"><?php echo __('Select Employee'); ?></label>
                                <div class="col-sm-8">
                                   <?php
                                        echo $this->Form->input('schedules.0.employee_id',
                                                array(
                                                        'class' => 'form-control',
                                                        'placeholder' => __(''),
                                                        'div' => false,
                                                        'label' => false,
                                                        'options' => $employee
                                                    )
                                            );
                                    ?>
                                </div>
                            </div>
                            <div class="form-group append-div" id="item">
                                <label  class="col-sm-4 control-label"><?php echo __('Select Item'); ?></label>
                                <div class="col-sm-8">
                                   <?php
                                        echo $this->Form->input('ScheduleItems.item_id',
                                                array(
                                                        'class' => 'form-control',
                                                        'placeholder' => __(''),
                                                        'div' => false,
                                                        'label' => false,
                                                        'name' => 'schedules[0][schedule_items][][item_id]',
                                                        'options' => $item
                                                    )
                                            );
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                             <div class="col-sm-1 plus"><span class ="glyphicon glyphicon-plus add"></span></div>
                             </div>
                            <div class="form-group">
                                <label  class="col-sm-4 control-label"><?php echo __('Schedule'); ?></label>
                                <div class="col-sm-8">
                                    <i class="fa fa-calendar cal-posi " aria-hidden="true"></i>
                                    <?php
                                        echo $this->Form->input(
                                                'schedules.0.date', [
                                                    'label' => false,
                                                    'type' =>'text',
                                                    'div' => false,
                                                    'placeholder' => __('Select'),
                                                    'class' => 'form-control date-picker'
                                                ]
                                            );
                                    ?>
                                </div>
                            </div>
                            <div class="form-group bordered_group"> 
                                <label  class="col-sm-4 control-label"></label>
                                <div class="col-lg-8">  
                                    <?php
                                        echo $this->Form->button(__('Submit'), [
                                                'type' => 'submit',
                                                'class' => 'btn submit-info submit_black'
                                            ]
                                        );
                                    ?>
                                    <?php
                                        echo $this->Form->button(__('Cancel'), [
                                                'type' => 'button',
                                                'class' => 'btn submit-info submit_black',
                                                'data-dismiss' => 'modal'
                                            ]
                                        );
                                    ?>
                                </div>
                            </div>
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>
                <div class="tab-pane fade" id="profile"> <div class="modal-body">
                    <h5 class="text-center"><?php echo __('Existing Client Order'); ?></h5>
                    <?php
                        echo $this->Form->create($scheduleObj,
                                [
                                    'id' => 'schedule-to-existing-client',
                                    'class' => 'form-horizontal',
                                    'url' => [
                                        'controller' => 'Clients',
                                        'action' => 'addScheduleToExistingClient'
                                    ]
                                ]
                            );
                    ?>
                        <div class="form-group">
                            <label  class="col-sm-4 control-label"><?php echo __('Client Full Name'); ?></label>
                            <div class="col-sm-8">
                                <?php
                                    echo $this->Form->input('client_id',
                                            [
                                                'class' => 'custom-select form-control',
                                                'options' => $clients,
                                                'label' => false
                                            ]
                                        );
                                ?>
                            </div>
                        </div>
                        <div class="form-group">
                            <label  class="col-sm-4 control-label"><?php echo __('Type Of Order'); ?></label>
                            <div class="order">
                                <div class="col-sm-2">
                                    <div class="radio">
                                        <label> 
                                            <?php
                                                echo $this->Form->radio('order_type', [
                                                        [
                                                            'value' => 1,
                                                            'text' => __('PS')
                                                        ]
                                                    ]);
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="col-sm-2 order">
                                    <div class="radio"> 
                                        <label>
                                            <?php
                                                echo $this->Form->radio('order_type', [
                                                        [
                                                            'value' => 2,
                                                            'text' => __('PIF')
                                                        ]
                                                    ]);
                                            ?>
                                        </label>
                                    </div>
                                </div>
                                <div class="order-type-err-msg"></div>
                            </div>
                        </div>
                        <div class="form-group">
                                <label  class="col-sm-4 control-label"><?php echo __('Select Employee'); ?></label>
                                <div class="col-sm-8">
                                   <?php
                                        echo $this->Form->input('employee_id',
                                                array(
                                                        'class' => 'form-control',
                                                        'placeholder' => __(''),
                                                        'div' => false,
                                                        'label' => false,
                                                        'options' => $employee
                                                    )
                                            );
                                    ?>
                                </div>
                            </div>
                            <div class="form-group append-item" id="item">
                                <label  class="col-sm-4 control-label"><?php echo __('Select Item'); ?></label>
                                <div class="col-sm-8">
                                   <?php
                                        echo $this->Form->input('ScheduleItems.item_id',
                                                array(
                                                        'class' => 'form-control',
                                                        'placeholder' => __(''),
                                                        'div' => false,
                                                        'label' => false,
                                                        'name' => 'schedule_items[][item_id]',
                                                        'options' => $item
                                                    )
                                            );
                                    ?>
                                </div>
                            </div>
                        <div class="form-group">
                             <div class="col-sm-1 plusitem"><span class ="glyphicon glyphicon-plus additem"></span></div>
                             </div>
                        <div class="form-group">
                            <label  class="col-sm-4 control-label"><?php echo __('Schedule'); ?></label>
                            <div class="col-sm-8">
                                <i class="fa fa-calendar cal-posi " aria-hidden="true"></i>
                                <?php
                                    echo $this->Form->input(
                                            'date', [
                                                'label' => false,
                                                'type' =>'text',
                                                'div' => false,
                                                'placeholder' => __('Select'),
                                                'class' => 'form-control date-picker'
                                            ]
                                        );
                                ?>
                            </div>
                        </div>
                        <div class="form-group bordered_group">
                            <label  class="col-sm-4 control-label"></label>
                            <div class="col-lg-8">
                                <?php
                                    echo $this->Form->button(__('Submit'),
                                            [
                                                'type' => 'submit',
                                                'data-toggle' => 'modal',
                                                'class' => 'btn submit-info submit_black'
                                            ]
                                        );
                                ?>
                                </button>
                                <?php
                                    echo $this->Form->button(__('Cancel'),
                                            [
                                                'data-dismiss' => 'modal',
                                                'class' => 'btn submit-info submit_black'
                                            ]
                                        );
                                ?>
                            </div>
                        </div>
                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<?php echo $this->Html->script('client/client',array('block' => 'scriptBottom'));?>