<div class="modal Educational-info schedule fade" id="edit-schedule-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <a href="#" class="border_radius" data-toggle="modal" data-dismiss="modal">X</a>
            <div class="modal-header">
                <h4 class="modal-title text-center"><?php echo __('Client Order'); ?></h4>
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#home" data-toggle="tab"><?php echo __('Edit Schedule'); ?></a></li>
                </ul>
            </div>
            <div class="tab-content">
                <div class="tab-pane fade active in">
                    <div class="modal-body">
                       <h4 class="modal-title text-center"><?php echo __('Edit Client Order'); ?></h4>
                        <?php
                            echo $this->Form->create($clientObj,
                                    [
                                        'id' => 'edit-schedule',
                                        'class' => 'form-horizontal',
                                        'url' => [
                                            'controller' => 'Clients',
                                            'action' => 'editScheduleData'
                                        ]
                                    ]
                                );
                        ?>
                            <div id="inner-edit-schedule-area">
                                
                            </div>
                        <?php echo $this->Form->end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<script type="text/html" id="editSchedule">
    <input type="hidden" name="id" value="<%= data.schedule.id %>">
    <input type="hidden" name="client[id]" value="<%= data.schedule.client.id %>">
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Type Of Order'); ?></label>
        <div class="col-sm-2">
            <div class="radio">
                <label>
                    <input type="radio" name="order_type" value="1" <% if(data.schedule.order_type == '1') { %>  checked <% } %> > <?php echo __('PS'); ?>
                </label> 
            </div>
        </div>
        <div class="col-sm-2">
            <div class="radio "> 
                <label>
                    <input type="radio" name="order_type" value="2" <% if(data.schedule.order_type == '2') { %> checked <% } %> > <?php echo __('PIF'); ?>
                </label> 
            </div>
        </div>
    </div>
    <div class="form-group">
        <label  class="col-sm-4 control-label"><?php echo __('Client Full Name'); ?></label>
        <div class="col-sm-8">
            <input type="text" name="client[name]" class="form-control" value="<%= data.schedule.client.name %>" >
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Phone Number'); ?></label>
        <div class="col-sm-8">
            <input type="text" name="client[phone]" class="form-control" value="<%= data.schedule.client.phone %>" >
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Facebook URL'); ?></label>
        <div class="col-sm-8">
            <input type="text" name="client[fb_url]" class="form-control" value="<%= data.schedule.client.fb_url %>" >
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-4 control-label"><?php echo __('Address'); ?></label>
        <div class="col-sm-8">
            <input type="text" name="client[address]" class="form-control" value="<%= data.schedule.client.address %>" >
        </div>
    </div>
    <div class="form-group">
        <label  class="col-sm-4 control-label"><?php echo __('Zip Code'); ?></label>
        <div class="col-sm-8">
           <input type="text" name="client[zipcode]" class="form-control" value="<%= data.schedule.client.zipcode %>" >
        </div>
    </div>
    <div class="form-group">
        <label  class="col-sm-4 control-label"><?php echo __('Email Address'); ?></label>
        <div class="col-sm-8">
           <input type="text" name="client[email]" class="form-control" value="<%= data.schedule.client.email %>" >
        </div>
    </div>
    <div class="form-group">
        <label  class="col-sm-4 control-label"><?php echo __('Select Employee'); ?></label>
            <div class="col-sm-8">
               <?php
                    echo $this->Form->input('schedules.0.employee_id',
                            array(
                                    'class' => 'form-control',
                                    'placeholder' => __(''),
                                    'div' => false,
                                    'label' => false,
                                    'default' => 6,
                                    'options' => $employee
                                )
                        );
                ?>
            </div>
        </div>
    <div class="form-group append-div" id="item">
            <label  class="col-sm-4 control-label"><?php echo __('Select Item'); ?></label>
                <div class="col-sm-8">
                   <?php
                        echo $this->Form->input('ScheduleItems.item_id',
                                array(
                                        'class' => 'form-control',
                                        'placeholder' => __(''),
                                        'div' => false,
                                        'label' => false,
                                        'name' => 'schedules[0][schedule_items][][item_id]',
                                        'options' => $item
                                    )
                            );
                    ?>
                </div>
    </div>
    <div class="form-group">
        <div class="col-sm-1 plusitem"><span class ="glyphicon glyphicon-plus additem"></span></div>
    </div>
    <div class="form-group">
        <label  class="col-sm-4 control-label"><?php echo __('Schedule'); ?></label>
        <div class="col-sm-8">
            <i class="fa fa-calendar cal-posi " aria-hidden="true"></i>
            <input type="text" name="date" class="form-control date-picker" value="<%= data.schedule.date %>" >
        </div>
    </div>
    <div class="form-group bordered_group"> 
        <label  class="col-sm-4 control-label"></label>
        <div class="col-lg-8">
            <button class="btn submit-info submit_black" type="submit"><?php echo __('Submit'); ?></button>
            <button class="btn submit-info submit_black" type="button" data-dismiss="modal"><?php echo __('Cancel'); ?></button>
            ?>
        </div>
    </div>
</script>
<?php echo $this->Html->script('client/client',array('block' => 'scriptBottom'));?>