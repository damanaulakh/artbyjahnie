<div class="content-wrapper">
    <?php echo $this->Flash->render('positive');?>
        <section class="content-header clearfix">
            <div class="col-lg-12 heading-top">
                <h1 class="heading-text-color pull-left">Edit CmsPage</h1>
            </div>
        </section>
<section class="content">
       <div class="box">         
          <div class="col-lg-12">                     
            <div id="emails">
     <?= $this->Form->create($cmsPage,array('class' => 'form-horizontal')) ?>
                     <div class="form-group">
                       <label class="control-label" for="exampleInputEmail1">Meta Title</label>
                       <?= $this->Form->input('meta_title', ['class'=>'form-control', 'label' => false]) ?>
                    </div>
                    <div class="form-group">
                    <?php echo $this->CKEditor->loadJs(); ?>
                       <label class="control-label" for="exampleInputEmail1">Meta Description</label>
                       <?= $this->Form->input('meta_description', ['class'=>'form-control', 'label' => false]) ?>
                       <?= $this->CKEditor->replace('meta_description'); ?>
                    </div>
                    <div class="form-group">
                       <label class="control-label" for="exampleInputEmail1">Meta Keyword</label>
                       <?= $this->Form->input('meta_keyword', ['class'=>'form-control', 'label' => false]) ?>
                    </div>
                    <div class="form-group">
                       <label class="control-label" for="exampleInputEmail1">Page Title</label>
                       <?= $this->Form->input('page_title', ['class'=>'form-control', 'label' => false]) ?>
                    </div>
                    <div class="form-group">
                       <label class="control-label" for="exampleInputEmail1">Page Name</label>
                       <?= $this->Form->input('page_name', ['class'=>'form-control', 'label' => false]) ?>
                    </div>
                    <div class="form-group">
                       <label class="control-label" for="exampleInputEmail1">Is Activated</label>
                       <?= $this->Form->input('is_activated', ['class'=>'form-control', 'label' => false]) ?>
                    </div>
                    <div class="col-lg-8">
                        <?= $this->Form->button(__('Submit'), ['class' => "sbtn submit-info submit_black", 'type' => 'submit']) ?>
                        <button class="btn submit-info submit_black">Cancel</button>
                    </div>
                <?= $this->Form->end() ?>
            </div>
        </div>
    </div> 
    </section>
</div>