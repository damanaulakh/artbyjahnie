<div class="content-wrapper">
    <?php echo $this->Flash->render('positive');?>
        <section class="content-header clearfix">
            <div class="col-lg-12 heading-top">
                <h1 class="heading-text-color pull-left">Manage CmsPages</h1>
            </div>
        </section>

 <section class="content shoping-cart clearfix">
            <div class="col-sm-12">
                <div class="note-listing">
                    <div class="table-responsive">
                        <table class="table shoping-cart-table">
                            <thead>
                                <tr>
                                    <th><?= $this->Paginator->sort('id') ?></th>
                                    <th><?= $this->Paginator->sort('meta_title') ?></th>
                                    <th><?= $this->Paginator->sort('meta_keyword') ?></th>
                                    <th><?= $this->Paginator->sort('page_title') ?></th>
                                    <th><?= $this->Paginator->sort('page_name') ?></th>
                                    <th><?= $this->Paginator->sort('created') ?></th>
                                    <th><?= $this->Paginator->sort('modified') ?></th>
                                    <th class="actions"><?= __('Actions') ?></th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php if(!empty($cmsPages->toArray())){
                                 foreach ($cmsPages as $cmsPage){ ?>
                                    <tr>
                                        <td><?= $this->Number->format($cmsPage->id) ?></td>
                                        <td><?= h($cmsPage->meta_title) ?></td>
                                        <td><?= h($cmsPage->meta_keyword) ?></td>
                                        <td><?= h($cmsPage->page_title) ?></td>
                                        <td><?= h($cmsPage->page_name) ?></td>
                                        <td><?= h($cmsPage->created) ?></td>
                                        <td><?= h($cmsPage->modified) ?></td>
                                        <td class="actions">
                                            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $cmsPage->id]) ?>
                                        </td>
                                     </tr>
                                     <?php } } else {?>
                                        <tr>
                                            <td colspan="5">
                                                No Record Found
                                            </td>
                                        </tr>
                                        <?php } ?>
                             </tbody>
                        </table>
                        <?php echo $this->element('pagination'); ?>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
