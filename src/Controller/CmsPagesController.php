<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\Network\Exception\NotFoundException;
use Cake\Datasource\Exception\RecordNotFoundException;

/**
 * CmsPages Controller
 *
 * @property \App\Model\Table\CmsPagesTable $CmsPages
 */
class CmsPagesController extends AppController
{
    public $helpers = ['AkkaCKEditor.CKEditor'];
    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {   
        
        parent::initialize();
        $this->loadComponent('Paginator');
        $this->viewBuilder()->layout('frontend'); 
        
    }
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {   $this->set('title', 'CmsPages');
        try {
            $query = $this->CmsPages->find()
                    ->where(['CmsPages.is_deleted' => false]);
            $cmsPages = $this->paginate($query);            
        } catch (NotFoundException $e) {
            // redirecting to Last page if request page doesn't exist
            $this->request->query['page'] = $this->request->query['page'] -1;
            return $this->redirect([
                    'controller' => $this->request->params['controller'],
                    'action' => $this->request->params['action'],
                    '?' => $this->request->query
                ]
            );
        } 
        $this->set(compact('cmsPages'));
        $this->set('_serialize', ['cmsPages']);
    }

    /**
     * View method
     *
     * @param string|null $id cms Page id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null) {       
        try {       
            $cmsPage = $this->CmsPages->get($id, [
                'contain' => []
            ]); 
        } catch (RecordNotFoundException $e) { 
            $this->Flash->error('Record not found please try agian');
            return $this->redirect($this->referer());
        }

        $this->set('cmsPage', $cmsPage);
        $this->set('_serialize', ['cmsPage']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $cmsPage = $this->CmsPages->newEntity();
        if ($this->request->is('post')) {
            $cmsPage = $this->CmsPages->patchEntity($cmsPage, $this->request->data);
            if ($this->CmsPages->save($cmsPage)) {
                $this->Flash->success(__('The cms page has been saved.'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The cms page could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('cmsPage'));
        $this->set('_serialize', ['cmsPage']);
    }

    /**
     * Edit method
     *
     * @param string|null $id cms Page id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $this->set('title', 'Edit Cms Page');
        try {       
            $cmsPage = $this->CmsPages->get($id, [
                'contain' => []
            ]); 
        } catch (RecordNotFoundException $e) { 
            $this->Flash->error('Record not found please try agian');
            return $this->redirect($this->referer());
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $cmsPage = $this->CmsPages->patchEntity($cmsPage, $this->request->data);
            if ($this->CmsPages->save($cmsPage)) {
                $this->Flash->success(__('The cms page has been saved.'));
                return $this->redirect(['action' => 'index']);
            }
        }
        $this->set(compact('cmsPage'));
        $this->set('_serialize', ['cmsPage']);
    }

    /**
     * Delete method
     *
     * @param string|null $id cms Page id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);        
        $cms = $this->CmsPages->get($id);
        $cms->is_deleted = ($cms->is_deleted) ? 0: 1;
        if ($this->CmsPages->save($cms)) {
            $this->Flash->success(__(
                            'Cms page: {0} has been  {1} successfully.', 
                            h($cms->page_title),
                            ($cms->is_deleted) ?'deleted': 'restored'
                        )
                    );
            return $this->redirect($this->referer());
        } else {
            $this->Flash->error(__(
                            'Cms page: {0} could not be  {1} please try again.', 
                            h($cms->page_title),
                            ($cms->is_deleted) ?'restored':'deleted'
                        )
                    );
            return $this->redirect($this->referer());
        }
    }
}
