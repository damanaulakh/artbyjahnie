<?php
namespace App\Controller;
use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\Datasource\Exception\RecordNotFoundException;
/**
 * EmailTemplates Controller
 *
 * @property \App\Model\Table\EmailTemplatesTable $EmailTemplates
 */
class EmailTemplatesController extends AppController
{
    public $helpers = ['AkkaCKEditor.CKEditor'];
    public $paginate = [
        'limit' => 10
    ];
    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {   
        $this->set('title', 'Email Templates');
        $this->viewBuilder()->layout('frontend');
        $whereCondition = ['is_deleted' => false];
        if (!empty($this->request->query['search']) ) {
            $search = [
                'or' => [
                    'template_used_for LIKE ' => '%'. $this->request->query['search'] .'%',
                    'subject LIKE ' => '%'. $this->request->query['search'] .'%',
                ]
            ];
            $whereCondition = array_merge($search, $whereCondition);
        }
        try {
            $query = $this->EmailTemplates->find()
                    ->where($whereCondition);
            $emailTemplates = $this->paginate($query);
            $this->set(compact('emailTemplates'));
            $this->set('_serialize', ['emailTemplates']);
        } catch (NotFoundException $e) {
            // redirecting to Last page if request page doesn't exist
            $this->request->query['page'] = $this->request->query['page'] -1;
            return $this->redirect([
                    'controllaer' => $this->request->params['controller'],
                    'action' => $this->request->params['action'],
                    '?' => $this->request->query
                ]
            );
        }         
    }

    /**
     * View method
     *
     * @param string|null $id Email Template id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $emailTemplate = $this->EmailTemplates->get($id, [
            'contain' => []
        ]);

        $this->set('emailTemplate', $emailTemplate);
        $this->set('_serialize', ['emailTemplate']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->viewBuilder()->layout('frontend');
        $emailTemplate = $this->EmailTemplates->newEntity();
        if ($this->request->is('post')) {
            $emailTemplate = $this->EmailTemplates->patchEntity($emailTemplate, $this->request->data);
            if ($this->EmailTemplates->save($emailTemplate)) {
                $this->Flash->success(__('The email template has been saved.'),
                    array('key' => 'positive'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The email template could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('emailTemplate'));
        $this->set('_serialize', ['emailTemplate']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Email Template id.
     * @return \Cake\Network\Response|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {   $this->set('title', 'Edit Email Templates');
        $this->viewBuilder()->layout('frontend');
        $emailTemplate = $this->EmailTemplates->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $emailTemplate = $this->EmailTemplates->patchEntity($emailTemplate, $this->request->data);
            if ($this->EmailTemplates->save($emailTemplate)) {
                $this->Flash->success(__('The email template has been saved.'),array('key' => 'positive'));
                return $this->redirect(['action' => 'index']);
            } else {
                $this->Flash->error(__('The email template could not be saved. Please, try again.'));
            }
        }
        $this->set(compact('emailTemplate'));
        $this->set('_serialize', ['emailTemplate']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Email Template id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $emailTemplate = $this->EmailTemplates->get($id);
        if ($this->EmailTemplates->delete($emailTemplate)) {
            $this->Flash->success(__('The email template has been deleted.'));
        } else {
            $this->Flash->error(__('The email template could not be deleted. Please, try again.'));
        }
        return $this->redirect(['action' => 'index']);
    }
}
