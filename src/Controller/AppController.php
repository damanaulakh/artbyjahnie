<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Mailer\Email;
use Cake\Core\Configure;
use Cake\Utility\Hash;
use Cake\I18n\I18n;
use Cake\ORM\TableRegistry;
use Twilio\Rest\Client;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');

        $this->loadComponent('Auth', [
            'loginAction' => [
                'controller' => 'Users',
                'action' => 'login'
            ],
            'logoutRedirect' => [
                'controller' => 'Users',
                'action' => 'login',
            ],
            'loginRedirect' => [
                'controller' => 'Users',
                'action' => 'employees',
            ],
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'email', 'password' => 'password']
                ]
            ]
        ]);
        if ($this->request->session()->check('Config.language')) {
            I18n::locale($this->request->session()->read('Config.language'));

        } 
    }

  
    
    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }
    public function _sendEmailMessage($to = null, $email_body = null, $subject = null, $bcc = null, $cc = null)
    {
        $email = new Email();
        $email->from([Configure::read('EmailFrom') => Configure::read('EmailFromName')])
            ->to($to)
            ->emailFormat('html')
            ->subject($subject);
        if ($email->send($email_body)) {
            return true;
        }
        return false;
    }

    protected function _setUser() {
        $this->loadModel('Users');
        $user = $this->Users->find()
            ->where(['Users.id' => $this->Auth->user('id')])
            ->first();
        $this->set(compact('user'));
        if($this->request->is('ajax')){
            $this->autoRender = false;
            $this->render('/Element/frontend/profile');
        } 
    }

/**
 * Method _setValidationErrorForInvoice to set validation errors for invoice
 *
 * @param $errors array containing the error list
 * @return $str string containg the list of errors as string
 */
    protected function _setValidationError($errors = array()) {
        $allErrors = Hash::flatten($errors);
        $str = null;
        if (!empty($allErrors)) {
            $str = '<ul>';
            foreach ($allErrors as $key => $val):
                $str.= '<li>'.$val.'</li>';
            endforeach;
            $str .= '</ul>';
        }
        return $str;
    }

/**
 * Method _sendSms to send text message
 *
 * @param $to string phone number to where the text message will go
 * @param $body string the body of the message
 * @return void
 */
    protected function _sendSms($to = null, $body = null) {
        $account_sid = Configure::read('Twilio.Sid');
        $auth_token = Configure::read('Twilio.Token');
        $client = new Client($account_sid, $auth_token);
         
        $from = Configure::read('Twilio.From');
        try{
            $client->messages->create(
                // the number you'd like to send the message to
                $to,
                array(
                    // A Twilio phone number you purchased at twilio.com/console
                    'from' => $from,
                    // the body of the text message you'd like to send
                    'body' => $body
                )
            );
            return true;
        }catch(SocketException $e){
            pr($e);die;
        }
    }
}