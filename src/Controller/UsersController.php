<?php
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\ORM\TableRegistry;
use Cake\Auth\DefaultPasswordHasher;
use Cake\Mailer\Email;
use Cake\Utility\Text;
/**
 * Static content controller
 *
 * This controller will render views from Template/Users/
 *
 */
class UsersController extends AppController
{
    public $paginate = [
       'limit' => 10
    ];
    /**
     * Displays a login view
     *
     * @return void|\Cake\Network\Response
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function login()
    {
        $this->viewBuilder()->layout('login');
        $user = TableRegistry::get('Users');
        if ($this->request->is('post')) {
            $user = $this->Auth->identify();
            if ($user) {
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            } else {
                $this->Flash->error(__('Username or password is incorrect'), [
                    'key' => 'positive'
                ]);
            }
        }
        $this->set('user');
    }

    public function logout()
    {
        return $this->redirect($this->Auth->logout());
    }

    public function employees() {
        $this->viewBuilder()->layout('frontend');
        $whereCondition = [
                          'Users.user_role_id' => 2
                        ];
        if (!empty($this->request->query['search'])) {
            $search = [
                'or' => [
                    'first_name LIKE ' => '%'. $this->request->query['search'] .'%',
                    'last_name LIKE ' => '%'. $this->request->query['search'] .'%'
                ]
            ];
                $whereCondition = array_merge($search, $whereCondition);
          }
            try {
               $query = $this->Users->find('all')
                       ->where($whereCondition);
                       
               $user = $this->paginate($query);            
            } catch (NotFoundException $e) {
               // redirecting to Last page if request page doesn't exist
               $this->request->query['page'] = $this->request->query['page'] -1;
               return $this->redirect([
                       'controller' => $this->request->params['controller'],
                       'action' => $this->request->params['action'],
                       '?' => $this->request->query
                   ]
               );
           }
        $this->set(compact('department','user'));

    }
    public function addEmployee() {
        $this->viewBuilder()->layout('frontend');
        $user = $this->Users->newEntity();       
        $user = $this->Users->patchEntity($user, $this->request->data);
        // pr($user);die;
        if ($this->Users->save($user)) {    
        $this->Flash->success('Subadmin has been added sucessfully', array(
                                                                    'key' => 'positive'
                                                                )
                                        ); 
        
          }
        else {
        $this->Flash->success('Subadmin has not been added sucessfully', array(
                                                                    'key' => 'positive'
                                                                )
                                        );
      }
    return $this->redirect($this->referer());
    $this->set('user');
    }
    public function editEmployee($id) {
        $this->viewBuilder()->layout = false;
        $this->loadModel('Users');
        $user = $this->Users->find()
            ->where(['Users.id' => base64_decode($id)])
            ->first();
        $this->set(compact('user'));
        if($this->request->is('ajax')){
            $this->autoRender = false;
            $this->render('/Element/frontend/edit_employee');
        }   
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->newEntity();
           $user = $this->Users->patchEntity($user, $this->request->data);
           //pr($user);die;
           if ($this->Users->save($user)) {
               $this->Flash->success(__('Subadmin has been Updated.'),array(
                                                                    'key' => 'positive'
                                                                ));
               return $this->redirect($this->referer());
           }
       }
    }
    public function deactivateEmployee($id){
     
       $user = $this->Users->get(base64_decode($id));
       $user->is_active = ($user->is_active) ? 0: 1;
       if ($this->Users->save($user)) {
           $this->Flash->success(__(
                       'The Subadmin with Email : {0} has been  {1} successfully.',
                       h($user->email),
                       ($user->is_activated) ? 'Activated': 'Deactivated'
                   ),
                array(
                                                                    'key' => 'positive'
                                                                )
               );
           //return $this->redirect($this->referer());
       }
    }
    public function changePassword() {
      $this->viewBuilder()->layout('frontend');
        if ($this->request->is('post')  && !empty($this->request->data('password'))) {          
            if($this->request->data('password') !== $this->request->data('confirm_password')) {
               $this->Flash->success('Password and confirm password did not matched', array(
                                                                    'key' => 'positive'
                                                                )
                                        ); 
               return $this->redirect($this->referer());
            }
           $users = $this->Users->get($this->Auth->user('id'));          
           if ($users->password != (new DefaultPasswordHasher)->check(
                   $this->request->data('old_password'),
                   $users->password
               )) {                
               $this->Flash->success('Old password do not match', array(
                                                                    'key' => 'positive'
                                                                )
                                        ); 
               return $this->redirect($this->referer());
           }
           $updateStatus = $this->Users->updateUserRecord(
               array('password'), $users['id'],
               array('password' => $this->request->data('password') )
           );
           if ($updateStatus) {
               $this->Flash->success('Password has been updated succesfully.',array(
                                                                    'key' => 'positive'
                                                                ));
               return $this->redirect($this->referer());
           } else {
               $this->Flash->error('Something went wrong. Please try again.');
               return $this->redirect($this->referer());
           }
        }
    }
    public function Profile() {
        $this->_setUser();
        if ($this->request->is(['patch', 'post', 'put'])) {
          if($this->request->data['profile_pic']['error'] == 0) {
          $ext = explode('.', $this->request->data['profile_pic']['name']);
          $fileName = Text::uuid() . '.' . end($ext);
          move_uploaded_file($this->request->data['profile_pic']['tmp_name'],WWW_ROOT.'/img/profile/' . $fileName) ;
          $this->request->data['profile_pic'] = $fileName; 
          }
          $user = $this->Users->newEntity();
           $user = $this->Users->patchEntity($user, $this->request->data);
           $user->id = $this->Auth->user('id');
           if ($this->Users->save($user)) {
                $authData = $this->Auth->user();
                $authData['first_name'] = $user->first_name;
                $authData['last_name'] = $user->last_name;
                if(isset($fileName)){
                    $authData['profile_pic'] = $user->profile_pic;
                }
                $authData['email'] = $user->email;
                $this->Auth->setUser($authData);
                $this->Flash->success(__('Subadmin has been Updated.'),array(
                                                                    'key' => 'positive'
                                                                ));
               return $this->redirect($this->referer());
           }
       }
    }
}
