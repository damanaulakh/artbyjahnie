(function () {
  var addStatusValidate = function(ele){
    console.log($(ele));
    $(ele).validate({
      rules: {
        "name": {
                    required: true
                },
        "department_id" : {
                  required: true
                },
      },
       messages: {      
               "name": {
                    required: "Enter status"
                },
                "department_id" : {
                  required: "Select the department"
                },
       }
    });
  };
addStatusValidate('#add-status');
addStatusValidate('#edit-status');
$('.editStatus').on('click',function(){
    var url = $(this).attr('data-url');
    $.ajax({
               type:"GET",
               url:url,        
               success: function(data){
                   console.log(data);
                   $('#edit-status').find('.modal-body').html(data);
                   $('#edit-status').modal('show');
                               
               },
               
           });
    });
})();