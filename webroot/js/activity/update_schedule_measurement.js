(function () {

})();
    var measurementValidate = function(ele){
            $(ele).validate({
              rules: {
                "schedule_items[][height]": {
                    required: true,
                
                },
                "schedule_items[][width]": {
                    required: true,
                   
                },
                "schedule_items[][description]": {
                required: true,
                }
              },
               messages: { 
               "schedule_items[][height]": {
                    required: "Enter the height"
            
                },
                "schedule_items[][width]": {
                    required: "Enter the width"
                    
                },
                "schedule_items[][description]": {
                    required: "Enter the description",
                }
            }
        });
    };

      $.validator.addMethod("decimalnumber", function(value, element) {
 return this.optional(element) || /^\d+(?:\.\d\d?)?$/.test(value);
}, "Enter the number");  
    measurementValidate('#measure');
    $(document).on('submit', '.schedule-update-form', function(e) {
        e.preventDefault();
        var element = $(this);
        var url = element.attr('action');
        $.ajax({
            url: url,
            data: element.serialize(),
            type: 'POST',
            beforeSend: function() {
                element.parents('tr').find('.measurement-update-loader').removeClass('hide');
            },
            success: function(resp) {
                if (resp.response.type == 1) {
                    show_stack_bar_top('success',resp.response.msg, true,'pnotify-custom');
                    return true;
                }
                show_stack_bar_top('error',resp.response.msg, true,'pnotify-custom');
            },
            error: function(xhr) {
                show_stack_bar_top('error',xhr.responseText, true,'pnotify-custom');
                element.parents('tr').find('.measurement-update-loader').addClass('hide');
            },
            complete: function() {
                element.parents('tr').find('.measurement-update-loader').addClass('hide');
            },
        });
    });

 