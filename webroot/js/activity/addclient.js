(function () {
    var addScheduleValidate = function(ele){
            $(ele).validate({
              rules: {
                "name": {
                    required: true,
                    maxlength : 200
                },
                "phone" : {
                    required: true,
                    //digits : true,
                   // maxlength : 12
                },
                'fb_url': {
                    url: true
                },
                "address" : {
                    required: true,
                },
                "zipcode" : {
                    required: true,
                    digits:true,
                    maxlength :5
                },
                "email" : {
                    required: true,
                    email:true
                }
              },
               messages: { 
               "name": {
                    required: "Enter the name",
                    maxlength : "Name must not be more than 200 characters"
                },     
                "phone" : {
                    required: "Enter Phone number",
                    //digits : "Enter numbers only",
                    maxlength : "Number must not be greater than 10 digits"
                },
                'fb_url': {
                    url: 'Plesae enter valid facebook url'
                },
                "address" : {
                    required: "Enter the address"
                },
                "zipcode" : {
                    required: "Enter the Zipcode",
                    digits : "Enter numbers only",
                    maxlength : "Zipcode must not be greater than 5 digis"
                },
                "email" : {
                    required: "Enter the email",
                    email : "Enter valid email address"
                }
            }
        });
    };
    addScheduleValidate('#add-client');
      var phoneValidations = function() {

        var telInput = $("#phone, #edit-phone"),
          errorMsg = $("#error-msg"),
          validMsg = $("#valid-msg");

        // initialise plugin
        telInput.intlTelInput({
          utilsScript: "/js/utils.js",
          nationalMode: false,
          preferredCountries : ['jo']
        });

        var reset = function() {
          telInput.removeClass("error");
          errorMsg.addClass("hide");
          validMsg.addClass("hide");
        };

        // on blur: validate
        telInput.blur(function() {
          reset();
          if ($.trim(telInput.val())) {
            if (telInput.intlTelInput("isValidNumber")) {
              validMsg.removeClass("hide");
            } else {
              telInput.addClass("error");
              errorMsg.removeClass("hide");
            }
          }
        });

        // on keyup / change flag: reset
        telInput.on("keyup change", reset);
    }

    phoneValidations();
})();