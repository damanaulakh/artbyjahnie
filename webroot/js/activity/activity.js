(function () {
    $('.date-picker').datetimepicker({
    	 format: 'YYYY-MM-DD'
    });
    $(document).on('click','.add',function() {
            $('.append-div:first').after($('.append-div:first').clone());
            if($('.append-div').length == 2){
                $('.plus').after('<div class="col-sm-1 minus"><span class="glyphicon glyphicon-minus remove"></span></div>');
            }
            if($('.append-div').length < 2){
                $('.minus').remove();
            }

      });
    $(document).on('click','.remove',function() {
                $('.append-div:last').remove();
                if($('.append-div').length < 2){
                    $('.minus').remove();
                }
            
          });
    $(document).on('click','.additem',function() {
            $('.append-item:first').after($('.append-item:first').clone());
            if($('.append-item').length == 2){
                $('.plusitem').after('<div class="col-sm-1 minusitem"><span class="glyphicon glyphicon-minus removeitem"></span></div>');
            }
            if($('.append-item').length < 2){
                $('.minusitem').remove();
            }

      });
    $(document).on('click','.removeitem',function() {
                $('.append-item:last').remove();
                if($('.append-item').length < 2){
                    $('.minusitem').remove();
                }
            
          });
    $(document).on('click','.addedit',function() {
        var clone = $('.append-edit:first').clone();
        clone.find('#scheduleitems-item-id').attr('name', 'schedule_items[' + $('.append-edit').length + '][item_id]');
            $('.append-edit:last').after(clone);
            if($('.append-edit').length == 2){
                $('.plusedit').after('<div class="col-sm-1 minusedit"><span class="glyphicon glyphicon-minus removeedit"></span></div>');
            }
            if($('.append-edit').length < 2){
                $('.minusedit').remove();
            }

      });
    $(document).on('click','.removeedit',function() {
                $('.append-edit:last').remove();
                if($('.append-edit').length < 2){
                    $('.minusedit').remove();
                }
            
          });
      var phoneValidations = function() {

        var telInput = $("#phone, #edit-phone"),
          errorMsg = $("#error-msg"),
          validMsg = $("#valid-msg");

        // initialise plugin
        telInput.intlTelInput({
          utilsScript: "/js/utils.js",
          nationalMode: false,
          preferredCountries : ['jo']
        });

        var reset = function() {
          telInput.removeClass("error");
          errorMsg.addClass("hide");
          validMsg.addClass("hide");
        };

        // on blur: validate
        telInput.blur(function() {
          reset();
          if ($.trim(telInput.val())) {
            if (telInput.intlTelInput("isValidNumber")) {
              validMsg.removeClass("hide");
            } else {
              telInput.addClass("error");
              errorMsg.removeClass("hide");
            }
          }
        });

        // on keyup / change flag: reset
        telInput.on("keyup change", reset);
    }

    phoneValidations();
    $('.scheduleView').on('click',function(){
        var url = $(this).attr('data-url');
        $.ajax({
                   type:"GET",
                   url:url,        
                   success: function(data){
                       $('#schedule-view').find('.modal-body').html(data);
                       $('#schedule-view').modal('show');
                                   
                   },
                   
               });
    });

})();
 