(function () {
	var changePasswordValidate = function(ele){
		console.log($(ele));
		$(ele).validate({
			rules: {
				"old_password": {
                    required: true
                },
                "password": {
                    required: true,
                    minlength: 6
                },
                "confirm_password": {
                    equalTo: '#new-password'
                }
			},
		   messages: {		  
		
		   }
		});
	};
	changePasswordValidate('#change-password');
	$('#changePasswordModal').on('shown.bs.modal', function () {
	  	changePasswordValidate('#change-password');

	})
})();
