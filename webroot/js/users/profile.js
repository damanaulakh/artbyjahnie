(function () {
var addprofileValidate = function(ele){
    $(ele).validate({
      rules: {
                "profile_pic": {
                    extension: "jpeg|jpg|png"
                    
                },  
                "phone" : {
                  digits : true
                }, 
      },
       messages: {  
              "phone" : {
                  digits : "Enter only digits"
                } ,
              "profile_pic": {
                    extension: "Only jpg and  png images are supported"
                    
                },  
       }
    });
  };
addprofileValidate('#add-Profile');
$('.editprofile').on('click',function(){
    var url = $(this).attr('data-url');
    $.ajax({
               type:"GET",
               url:url,        
               success: function(data){
                   console.log(data);
                   $('#changeProfile').find('.modal-body').html(data);
                   $('#changeProfile').modal('show');
                               
               },
               
           });
    });
$('#phone').intlTelInput();
})();