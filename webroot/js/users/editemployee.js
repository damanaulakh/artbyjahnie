(function () {
    var addEmployeeValidate = function(ele){
        console.log($(ele));
        $(ele).validate({
            rules: {
                "first_name": {
                  required: true,
                        },
                "last_name" : {
                  required: true
                },
                "phone" : {
                  required: true,
                  // digits:true,
                  intlphone: true
                },
                "email" : {
                  required: true,
                  email : true
                },
                "department_id" : {
                  required: true,
                },
                "password" : {
                  required: true,
                },
                "confirm_password" :{
                    required : true,
                    equalTo : "#password"
                }
            },
            messages: { 
                "first_name": {
                  required: "Enter the First name",
                        },     
                "last_name" : {
                required: "Enter the Last name",
                },
                "phone" : {
                    required: "Enter the phone number"
                },
                "email" : {
                    required: "Enter the email",
                    email : "Enter valid email address"
                },
                "department_id" : {
                    required: "Select any one of the department",
                },
                "password" : {
                    required : "Enter Password"
                },
                "confirm_password" : {
                    required : "Enter the confirm password",
                    equalTo : "password and confirm password do no match"
                }
            }
        })
    };
    addEmployeeValidate('#add-employee');
    $('.editEmployee').on('click',function(){
        var url = $(this).attr('data-url');
        $.ajax({
                type:"GET",
                url:url,        
                success: function(data){
                    console.log(data);
                    $('#edit-employee').find('.modal-body').html(data);
                    $('#edit-employee').modal('show');
                    addEmployeeValidate('#edit-employee-form');
                    phoneValidations();
                },
               
            });
    });
    $(".switch_custom").on("change",function(){
        if ($(this).is(':checked')) {
          var url = $(this).attr('data-url')+"/1";
        } else {
          var url = $(this).attr('data-url')+"/0";
        }
        $.ajax({
                   type:"GET",
                   url : url,   
                   success: function(data){
                       console.log(data);
                      
                   },
                   
          });
  });

    var phoneValidations = function() {

        var telInput = $("#phone, #edit-phone"),
          errorMsg = $("#error-msg"),
          validMsg = $("#valid-msg");

        // initialise plugin
        telInput.intlTelInput({
          utilsScript: "/js/utils.js",
          nationalMode: false,
          preferredCountries : ['jo']
        });

        var reset = function() {
          telInput.removeClass("error");
          errorMsg.addClass("hide");
          validMsg.addClass("hide");
        };

        // on blur: validate
        telInput.blur(function() {
          reset();
          if ($.trim(telInput.val())) {
            if (telInput.intlTelInput("isValidNumber")) {
              validMsg.removeClass("hide");
            } else {
              telInput.addClass("error");
              errorMsg.removeClass("hide");
            }
          }
        });

        // on keyup / change flag: reset
        telInput.on("keyup change", reset);
    }

    phoneValidations();

})();