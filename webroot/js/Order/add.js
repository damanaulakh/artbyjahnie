$(function () {

    $('.quote-date').datetimepicker({
        //minDate: new Date(),
        format: 'YYYY-MM-DD'
    });
	//change event on select item while adding quote
    $(document).on('change','.quote-item-select',function() {
    	var element = $(this);
    	onChangeItem(element);
    });

    //on changing the item
    var onChangeItem = function (element) {
    	var item_id = element.val();
    	var url = element.data('url');
    	$.ajax({
    		url: url + '/' + item_id + '.json',
    		beforeSend: function() {
    			if (item_id == '') {
    				recalculate(element);
    				return false;
    			}
                element.parents('tr').find('.item-under-process').removeClass('hide');
            },
            success: function(resp) {
            	element.parents('tr').find('.item-desc').html(resp.response.description);
            },
            error: function(xhr) {
                show_stack_bar_top('error',xhr.responseText, true,'pnotify-custom');
                element.parents('tr').find('.item-under-process').addClass('hide');
            },
            complete: function() {
            	// $('.quote-unit-select').val('per_price');
            	var item_unit = element.parents('tr').find('.quote-unit-select').val();
            	if (item_unit != '') {
            		unitElement = element.parents('tr').find('.quote-unit-select');
            		onChangeItemUnit(unitElement);
            	}
                element.parents('tr').find('.item-under-process').addClass('hide');
            }
    	});
    }

    //getting item selected unit price
    $(document).on('change','.quote-unit-select', function() {
    	var element = $(this);
    	onChangeItemUnit(element);
    });

    $(document).on('change', '.item-quantity', function() {
    	var element = $(this).parents('tr').find('.quote-item-select');
    	if (element.val() == '') {
    		return false;
    	}
    	onChangeItem(element);
    });

    //on change of item unit select box
    var onChangeItemUnit = function(element) {
    	var unit_id = element.val();
    	var url = element.data('url');
    	var item_id = element.parents('tr').find('.quote-item-select').val();
        console.log(item_id);
    	$.ajax({
    		url: url + '/' + item_id + '/' + unit_id +  '/.json',
    		beforeSend: function() {
    			if (item_id == '') {
    				recalculate(element);
    				return false;
    			}
                element.parents('tr').find('.item-under-process').removeClass('hide');
            },
            success: function(resp) {
            	calculateItemTotal(element,unit_id,resp);
            },
            error: function(xhr) {
                show_stack_bar_top('error',xhr.responseText, true,'pnotify-custom');
                element.parents('tr').find('.item-under-process').addClass('hide');
            },
            complete: function() {
                element.parents('tr').find('.item-under-process').addClass('hide');
            }
    	});
    }

    //calculating item total price by applying arithmetical operation on unit price, quantity and tax.
    var calculateItemTotal = function(element,unit_id,item) {
    	var item_quantity = element.parents('tr').find('.item-quantity').val();
    	var item_unit_price = null;
    	switch(unit_id) {
            case 'per_price':
            	item_unit_price = item.response.per_price;
            	break;
            case 'per_meter':
            	item_unit_price = item.response.per_meter;
            	break;
            case 'meter_linear_meter':
            	item_unit_price = item.response.meter_linear_meter;
            	break;
            case 'per_linear_meter':
            	item_unit_price = item.response.per_linear_meter;
            	break;
           }
		element.parents('tr').find('.hidden-item-unit-price').val(item_unit_price);
    	var item_tax = element.parents('tr').find('.item-tax').val();
    	var item_quantity_into_price = parseInt(item_quantity) * parseInt(item_unit_price);
    	var item_price_total_tax = parseInt(item_quantity_into_price) * parseInt(item_tax) / 100;
    	var item_total_price = item_quantity_into_price + item_price_total_tax;
        console.log(item_tax);
    	element.parents('tr').find('.item-total-price').html('JOD ' + parseFloat(item_total_price).toFixed(2));
    	element.parents('tr').find('.hidden-item-total-price').val(parseFloat(item_total_price).toFixed(2));
    	calculatingSubTotal();

    	return true;
    }

    //recalculate total prices after selectnig item to empty
    var recalculate = function(element) {
    	element.parents('tr').find('.item-total-price').html('JOD 0.00');
		element.parents('tr').find('.item-desc').html('');
		element.parents('tr').find('.hidden-item-unit-price').val('0.00');
		element.parents('tr').find('.hidden-item-total-price').val('0.00');
		$('.quote-subtotal').html('JOD 0.00');
		calculatingSubTotal();
    }

    //calculating sub total and show
    var calculatingSubTotal = function() {
    	var sub_total = 0;
    	$('.items-listing').find('.hidden-item-total-price').each(function() {
             console.log($(this).val());
    		sub_total = (parseFloat(sub_total)+parseFloat($(this).val())).toFixed(2);
    	});
        console.log(sub_total);
    	$('.quote-subtotal').html('JOD ' + sub_total);
    	$('.quote-amount').val(sub_total);
    	var vat = (sub_total * 13) / 100;
    	$('.vat-on-quote').html('JOD ' + parseFloat(vat).toFixed(2));
    	$('.quote-vat').val(parseFloat(vat).toFixed(2));
    	var total = (parseFloat(sub_total)+parseFloat(vat)).toFixed(2);;
    	$('.price-vat-total').html('JOD ' + total);
    	$('.quote-sub-total').val(total);
    }

    var index = $('.items-listing > tr').length;
    $(document).on('click', '.add-new-item', function() {
    	var clone = $('.cloneable-row:first').clone();
    	clone.first('tr').removeAttr('class');

    	clone.find('.item-quantity').val('1');
    	clone.find('.item-quantity').attr('name', 'quote_items[' + index + '][quantity]');

    	clone.find('.quote-item-select').val('');
    	clone.find('.quote-item-select').attr('name', 'quote_items[' + index + '][item_id]');

    	clone.find('.quote-unit-select').val('per_price');
    	clone.find('.quote-unit-select').attr('name', 'quote_items[' + index + '][unit]');

    	clone.find('.hidden-item-unit-price').val('0.00');
    	clone.find('.hidden-item-unit-price').attr('name', 'quote_items[' + index + '][unit_price]');

    	clone.find('.item-tax').attr('name', 'quote_items[' + index + '][tax]');
        var a = clone.find('img').length;
        if(a != 0) {
            clone.find('img').closest('td').html('<a href="javascript:void(0)" data-id="'+index+'" id="br'+index+'" class="add_button file">Browse File</a>');
        }else{
            clone.find('.file').attr('data-id', index );

            clone.find('.file').attr('id', 'br' + index);
        }
        

    	clone.find('.item-total-price').html('JOD 0.00');

    	clone.find('.hidden-item-total-price').val('0.00');
    	clone.find('.hidden-item-total-price').attr('name', 'quote_items[' + index + '][total_price]');

    	clone.appendTo('.items-listing');
    	index++;
    });

    //remove items and calculates the total prices again
    $(document).on('click','.remove-item',function() {
    	if ($('.items-listing').find('tr').length > 1) {
	    	$('tbody.items-listing tr').last().remove();
	    	calculatingSubTotal();
    	}
    });

    //copying billing address to shipping adress
    $(document).on('change','.same-addresses',function() {
    	$('.shipping-address').val('');
    	if ($(this).is(':checked')) {
    		var billing_address = $('.billing-address').val();
    		$('.shipping-address').val(billing_address);
    	}
    });

    //event fires on submitting create quote form
    $(document).on('click','.submit-quote',function(e) {
    	e.preventDefault();
    	var element = $(this);
        if (element.hasClass('print')) {
            $('input[name="submitted_type"]').val('print');
        }
        var form = $('#save-order');
        var url = form.attr('action');
        $.ajax({
    		url: url + '.json',
    		data: form.serialize(),
    		type: 'post',
    		beforeSend: function() {
    			element.find('.save-quote-loader').removeClass('hide');
            },
            success: function(resp) {
            	if (resp.response.type == 0) {
            		show_stack_bar_top('error',resp.response.msg, true,'pnotify-custom');
            		return false;
            	}
                $quote_id = (resp.response.quote_id) ? window.btoa(resp.response.quote_id) : '';
            	window.location = resp.response.url + '/' + $quote_id;
            },
            error: function(xhr) {
                show_stack_bar_top('error',xhr.responseText, true,'pnotify-custom');
                element.find('.save-quote-loader').addClass('hide');
            },
            complete: function() {
            	element.find('.save-quote-loader').addClass('hide');
            }
    	});
    });

    $("body").addClass("sidebar-collapse");
    $('.checkedup li a').click(function(){
        $(this).closest('.checkedup').find('li.selected').removeClass('selected');
        $(this).parent('li').toggleClass('selected');
    });
     $('.add-pic').click(function(){
        var name = $('li.selected img').attr('data-name');
        var src = $('li.selected img').attr('src');
        var button = $('#'+$(this).attr('bid'));
        var buttonid = $(this).attr('data-id');
        $(button).closest('td').html('<img src = "' + src + '"> <input type="hidden" name="quote_items['+buttonid+'][file_path]" value='+name+'>');
        $('#add-image').modal('hide'); 
    });
    $(document).on('click','.file',function(){
            $('#add-image').modal('show'); 
            $('.add-pic').attr('bid',$(this).attr('id'));
            $('.add-pic').attr('data-id',$(this).attr('data-id'));                         
    });

});
