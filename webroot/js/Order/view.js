(function () {
    $('.orderView').on('click',function(){
        var url = $(this).attr('data-url');
        $.ajax({
                   type:"GET",
                   url:url,        
                   success: function(data){
                       $('#order-view').find('.modal-body').html(data);
                       $('#order-view').modal('show');
                                   
                   },
                   
               });
    });

    $(document).on('change','.change-order-log',function() {
        var element = $(this);
        var url = element.data('url');
        var status_id = element.val();
        $.ajax({
            url: url + '/' + status_id + '.json',
            beforeSend: function() {
                element.parents('tr').find('.order-status-under-process').removeClass('hide');
            },
            success: function(resp) {
                if (resp.response.type == 1) {
                    show_stack_bar_top('success',resp.response.msg, true,'pnotify-custom');
                    return true;
                }
                show_stack_bar_top('error',resp.response.msg, true,'pnotify-custom');
            },
            error: function(xhr) {
                show_stack_bar_top('error',xhr.responseText, true,'pnotify-custom');
                element.parents('tr').find('.order-status-under-process').addClass('hide');
            },
            complete: function() {
                element.parents('tr').find('.order-status-under-process').addClass('hide');
            }
        });
    });
})();