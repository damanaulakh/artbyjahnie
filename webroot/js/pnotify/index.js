function show_stack_bar_top(type,msg,flag,customclass) {
   var opts = {
      title: "Over Here",
      text: "Check me out. I'm in a different stack.",
      addclass: customclass,
      cornerclass: "",
      width:"70%",
      hide: flag,
      stack: {"dir1": "down", "dir2": "right", "push": "top", "spacing1": 10, "spacing2": 10}
   };
   switch (type) {
      case 'error':
         opts.title = "Error";
         opts.text = msg;
         opts.type = "error";
         break;
      case 'warning':
         opts.title = "Warning";
         opts.text = msg;
         opts.type = "Warning";
         break;
      case 'success':
         opts.title = "Success";
         opts.text = msg;
         opts.type = "success";
         break;
      }
   new PNotify(opts);
}