(function () {
  var addDepartmentValidate = function(ele){
    console.log($(ele));
    $(ele).validate({
      rules: {
        "name": {
                    required: true
                },
      },
       messages: {      
    
       }
    });
  };
  var addItemValidate = function(ele){
    console.log($(ele));
    $(ele).validate({
      rules: {
        "name": {
          required: true,
          maxlength : 200
                },
        "description" : {
          required: true
        },
        "per_price" : {
          required: true,
          digits:true
        },
        "per_meter" : {
          required: true,
          digits:true
        },
        "meter_linear_meter" : {
          required: true,
          digits:true
        },
        "per_linear_meter" : {
          required: true,
          digits:true
        }
      },
       messages: { 
       "name": {
          required: "Enter the item name",
          maxlength : "Item name must not be more than 200 characters"
                },     
            "per_price" : {
            required: "Enter the Per Price",
            digits : "Enter numbers only"
        },
        "per_meter" : {
            required: "Enter the Per Price",
            digits : "Enter numbers only"
        },
        "meter_linear_meter" : {
            required: "Enter the Per Price",
            digits : "Enter numbers only"
        },
        "per_linear_meter" : {
            required: "Enter the Per Price",
            digits : "Enter numbers only"
        }
       }
    });
  };
addDepartmentValidate('#add-department');
addItemValidate('#additem');
$('.editDepartment').on('click',function(){
    var url = $(this).attr('data-url');
    $.ajax({
               type:"GET",
               url:url,        
               success: function(data){
                   console.log(data);
                   $('#edit-department').find('.modal-body').html(data);
                   $('#edit-department').modal('show');
                               
               },
               
           });
    });
$('.editItem').on('click',function(){
    var url = $(this).attr('data-url');
    $.ajax({
               type:"GET",
               url:url,        
               success: function(data){
                   console.log(data);
                   $('#edit-item').find('.modal-body').html(data);
                   $('#edit-item').modal('show');
                               
               },
               
           });
    });
})();